#ifndef MSGPROCESSINGUTILITYTESTSTRINGS_H
#define MSGPROCESSINGUTILITYTESTSTRINGS_H

#include "GlobalUtilityMacros.h"

#include <QByteArray>
#include <QList>

namespace ProtocolStr
{

struct MsgProcessingUtilityTestStrings
{
    static const QList<QByteArray> jsons();

    Q_DISABLE_INSTANTIATION(MsgProcessingUtilityTestStrings);
};

}

#endif // MSGPROCESSINGUTILITYTESTSTRINGS_H
