/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "MsgProcessingUtilityFunctionsTestStrings.h"

const QList<QByteArray>
ProtocolStr::MsgProcessingUtilityTestStrings::jsons()
{

    QList<QByteArray> jsons;

    jsons.append( "{"
                  "\"menu\": {"
                  "\"id\": \"file\","
                  "\"value\": \"File\","
                  "\"popup\": {"
                  "\"menuitem\": ["
                  "{\"value\": \"New\", \"onclick\": \"CreateNewDoc()\"},"
                  "{\"value\": \"Open\", \"onclick\": \"OpenDoc()\"},"
                  "{\"value\": \"Close\", \"onclick\": \"CloseDoc()\"}"
                  "]"
                  "}"
                  "}"
                  "}"
                  );

    jsons.append( "{"
                  "\"first\": \"John\","
                  "\"last\": \"Doe\","
                  "\"age\": 39,"
                  "\"sex\": \"M\","
                  "\"salary\": 70000,"
                  "\"registered\": true"
                  "}"
                  );

    jsons.append( "{"
                  "\"first\": \"John\","
                  "\"last\": \"Doe\","
                  "\"age\": 39,"
                  "\"sex\": \"M\","
                  "\"salary\": 70000,"
                  "\"registered\": true,"
                  "\"favorites\": {"
                  "\"color\": \"Blue\","
                  "\"sport\": \"Soccer\","
                  "\"food\": \"Spaghetti\""
                  "}"
                  "}"
                  );

    return jsons;
}
