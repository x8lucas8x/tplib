/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "SetPlaylistRevisionCommandTest.h"

#include "SetPlaylistRevisionCommand.h"
#include "DatabaseCommandCommomTests.h"
#include "DatabaseCommandTestStrings.h"

SetPlaylistRevisionCommandTest::SetPlaylistRevisionCommandTest( QObject *parent )
    : QObject( parent )
    , m_command( SetPlaylistRevisionCommand::create() )
    , m_jsons( ProtocolStr::DatabaseCommandTestStrings::setPlaylistRevision() )
{
}

void
SetPlaylistRevisionCommandTest::init()
{
    CommandsCommomTests::testIfCommandIsInitialized( m_command,
                                                     DatabaseCommand::SetPlaylistRevisionCommand );
}

void
SetPlaylistRevisionCommandTest::testIfCommandTypeIsOk()
{
    CommandsCommomTests::testIfCommandTypeIsOk( m_command, m_jsons );
}

void
SetPlaylistRevisionCommandTest::testIfDatabaseCommandProducesValidJson()
{
    CommandsCommomTests::testIfDatabaseCommandProducesValidJson( m_command,
                                                                 m_jsons );
}

void
SetPlaylistRevisionCommandTest::testJsonToRenamePlaylistCommandConversion()
{
    CommandsCommomTests::testJsonToDatabaseCommandConversion( m_command,
                                                              m_jsons );
}
