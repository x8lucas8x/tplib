/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "DatabaseCommandTestStrings.h"

const QList<QByteArray>
ProtocolStr::DatabaseCommandTestStrings::addFiles()
{
    QList<QByteArray> jsons;

    jsons.append( "{"
                  "\"command\": \"addfiles\","
                  "\"guid\": \"7ea0b94a-498f-4c1a-a9ac-afb6b11d3a3d\","
                  "\"files\": ["
                  "{"
                  "\"id\": 100,"
                  "\"url\": \"1\","
                  "\"artist\": \"Bloc Party\","
                  "\"album\": \"Banquet\","
                  "\"track\": \"Banquet\","
                  "\"mimetype\": \"audio/mpeg\","
                  "\"hash\": \"\","
                  "\"year\": 2002,"
                  "\"albumpos\": 1,"
                  "\"mtime\": 1261233412,"
                  "\"duration\": 201,"
                  "\"bitrate\": 128,"
                  "\"size\": 3229824"
                  "}"
                  "]"
                  "}"
                  );

    return jsons;
}

const QList<QByteArray>
ProtocolStr::DatabaseCommandTestStrings::deleteFiles()
{
    QList<QByteArray> jsons;

    jsons.append( "{"
                  "\"command\": \"deletefiles\","
                  "\"guid\": \"f32a1bed-9774-48ec-b90f-71795fab94d8\","
                  "\"ids\": [351, 352, 353, 354]"
                  "}"
                  );

    return jsons;
}

const QList<QByteArray>
ProtocolStr::DatabaseCommandTestStrings::createPlaylist()
{
    QList<QByteArray> jsons;

    jsons.append( "{"
                  "\"command\": \"createplaylist\","
                  "\"guid\": \"235517ef-488d-4d35-a5c3-6e2092aa0edd\","
                  "\"playlist\": {"
                  "\"info\": \"\","
                  "\"creator\": \"My Collection\","
                  "\"createdon\": 1321308437,"
                  "\"title\": \"New Playlist\","
                  "\"currentrevision\": \"\","
                  "\"shared\": false,"
                  "\"guid\": \"d333b5f7-fda3-4f58-a387-18f47ca02b6d\""
                  "}"
                  "}"
                  );

    return jsons;
}

const QList<QByteArray>
ProtocolStr::DatabaseCommandTestStrings::renamePlaylist()
{
    QList<QByteArray> jsons;

    jsons.append( "{"
                  "\"command\": \"renameplaylist\","
                  "\"guid\": \"60f995a0-305e-49e4-a99c-b445f51a71af\","
                  "\"playlistguid\": \"d333b5f7-fda3-4f58-a387-18f47ca02b6d\","
                  "\"playlistTitle\": \"Greatest\""
                  "}"
                  );

    return jsons;
}

const QList<QByteArray>
ProtocolStr::DatabaseCommandTestStrings::setPlaylistRevision()
{
    QList<QByteArray> jsons;

    jsons.append( "{"
                  "\"command\": \"setplaylistrevision\","
                  "\"guid\": \"4181526a-1298-4130-8b1d-59eea3a8249a\","
                  "\"oldrev\": \"\","
                  "\"newrev\": \"012f4a07-0362-4db3-a886-4c7766a17889\","
                  "\"playlistguid\": \"d333b5f7-fda3-4f58-a387-18f47ca02b6d\","
                  "\"addedentries\": ["
                  "{"
                  "\"duration\": 4294967295,"
                  "\"lastmodified\": 0,"
                  "\"guid\": \"77b113ae-6ee0-4e63-bcd2-94cb3dc3990c\","
                  "\"annotation\": \"\","
                  "\"query\": {"
                  "\"album\": \"Yoshimi Battles the Pink Robots\","
                  "\"duration\": -1,"
                  "\"qid\": \"b8e24961-7041-48db-9f41-161f4aadf362\","
                  "\"track\": \"Yoshimi Battles the Pink Robots, Part 1\","
                  "\"artist\": \"The Flaming Lips\""
                  "}"
                  "},"
                  "{"
                  "\"duration\": 4294967295,"
                  "\"lastmodified\": 0,"
                  "\"guid\": \"12ef55df-cbe1-4756-be71-b2209bcd6199\","
                  "\"annotation\": \"\","
                  "\"query\": {"
                  "\"album\": \"Yoshimi Battles the Pink Robots\","
                  "\"duration\": -1,"
                  "\"qid\": \"0cf3bd28-35aa-4711-ab4e-182a92b55cfd\","
                  "\"track\": \"Yoshimi Battles the Pink Robots, Part 2\","
                  "\"artist\": \"The Flaming Lips\""
                  "}"
                  "}"
                  "],"
                  "\"orderedguids\": ["
                  "\"77b113ae-6ee0-4e63-bcd2-94cb3dc3990c\","
                  "\"12ef55df-cbe1-4756-be71-b2209bcd6199\""
                  "]"
                  "}"
                  );

    return jsons;
}

const QList<QByteArray>
ProtocolStr::DatabaseCommandTestStrings::deletePlaylist()
{
    QList<QByteArray> jsons;

    jsons.append( "{"
                  "\"command\": \"deleteplaylist\","
                  "\"guid\": \"54e6e9d4-aeb1-4ecc-9031-eff060ec0540\","
                  "\"playlistguid\": \"d333b5f7-fda3-4f58-a387-18f47ca02b6d\""
                  "}"
                  );

    return jsons;
}

const QList<QByteArray>
ProtocolStr::DatabaseCommandTestStrings::socialAction()
{
    QList<QByteArray> jsons;

    jsons.append( "{"
                  "\"command\": \"socialaction\","
                  "\"guid\": \"8675227b-f8f8-4cfa-9e38-b155383f1460\","
                  "\"action\": \"Love\","
                  "\"comment\": \"true\","
                  "\"artist\": \"Nirvana\","
                  "\"track\": \"The Man Who Sold The World\","
                  "\"timestamp\": 1321460771"
                  "}"
                  );

    return jsons;
}

const QList<QByteArray>
ProtocolStr::DatabaseCommandTestStrings::logPlayback()
{
    QList<QByteArray> jsons;

    jsons.append( "{"
                  "\"command\": \"logplayback\","
                  "\"secsPlayed\": 56,"
                  "\"artist\": \"Beastie Boys\","
                  "\"track\": \"Brass Monkey\","
                  "\"action\": 2,"
                  "\"playtime\": 1321454618,"
                  "\"guid\": \"a2fd565f-99a5-45c2-bb38-a881cfa20c20\""
                  "}"
                  );

    return jsons;
}
