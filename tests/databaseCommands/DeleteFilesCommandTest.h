/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef DELETEFILESCOMMANDTEST_H
#define DELETEFILESCOMMANDTEST_H

#include "DatabaseCommandsTypedefs.h"
#include "../AutoTester.h"

#include <QObject>

class DeleteFilesCommandTest : public QObject
{
    Q_OBJECT
public:
    DeleteFilesCommandTest( QObject *parent = 0 );

private slots:
  void init();

  void testIfCommandTypeIsOk();
  void testIfDatabaseCommandProducesValidJson();
  void testJsonToDeleteFilesCommandConversion();

private:
    DeleteFilesCommandPtr m_command;
    const QList<QByteArray> m_jsons;
};

DECLARE_TEST(DeleteFilesCommandTest)

#endif // DELETEFILESCOMMANDTEST_H
