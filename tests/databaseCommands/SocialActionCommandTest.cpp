/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "SocialActionCommandTest.h"

#include "SocialActionCommand.h"
#include "DatabaseCommandCommomTests.h"
#include "DatabaseCommandTestStrings.h"

SocialActionCommandTest::SocialActionCommandTest( QObject *parent )
    : QObject( parent )
    , m_command( SocialActionCommand::create() )
    , m_jsons( ProtocolStr::DatabaseCommandTestStrings::socialAction() )
{
}

void
SocialActionCommandTest::init()
{
    CommandsCommomTests::testIfCommandIsInitialized( m_command,
                                                     DatabaseCommand::SocialActionCommand );
}

void
SocialActionCommandTest::testIfCommandTypeIsOk()
{
    CommandsCommomTests::testIfCommandTypeIsOk( m_command, m_jsons );
}

void
SocialActionCommandTest::testIfDatabaseCommandProducesValidJson()
{
    CommandsCommomTests::testIfDatabaseCommandProducesValidJson( m_command,
                                                                 m_jsons );
}

void
SocialActionCommandTest::testJsonToRenamePlaylistCommandConversion()
{
    CommandsCommomTests::testJsonToDatabaseCommandConversion( m_command,
                                                              m_jsons );
}
