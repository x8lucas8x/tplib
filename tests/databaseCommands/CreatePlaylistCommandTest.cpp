/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "CreatePlaylistCommandTest.h"

#include "CreatePlaylistCommand.h"
#include "DatabaseCommandCommomTests.h"
#include "DatabaseCommandTestStrings.h"
#include "Playlist.h"
#include "MsgProcessingUtilityFunctions.h"

CreatePlaylistCommandTest::CreatePlaylistCommandTest( QObject *parent )
    : QObject( parent )
    , m_command( CreatePlaylistCommand::create() )
    , m_jsons( ProtocolStr::DatabaseCommandTestStrings::createPlaylist() )
{
}

void
CreatePlaylistCommandTest::init()
{
    CommandsCommomTests::testIfCommandIsInitialized( m_command,
                                                     DatabaseCommand::CreatePlaylistCommand );
}

void
CreatePlaylistCommandTest::testIfCommandTypeIsOk()
{
    CommandsCommomTests::testIfCommandTypeIsOk( m_command, m_jsons );
}

void
CreatePlaylistCommandTest::testIfDatabaseCommandProducesValidJson()
{
    CommandsCommomTests::testIfDatabaseCommandProducesValidJson( m_command,
                                                                 m_jsons );
}

void
CreatePlaylistCommandTest::testJsonToCreatePlaylistCommandConversion()
{
    CommandsCommomTests::testJsonToDatabaseCommandConversion( m_command,
                                                              m_jsons );
}

void
CreatePlaylistCommandTest::testDatabasePlaylistToPLaylistConversion()
{
    QVariant parsedJsonIn = MsgProcessingUtils::parseJson( m_jsons.first() );
    m_command->jsonToDatabaseCommand( parsedJsonIn.toMap() );

    TPCoreApi::PlaylistPtr playlist = m_command->playlist()->toPlaylist();
    m_command->playlist()->fromPlaylist( playlist );
    DatabasePlaylistPtr databasePlaylist = m_command->playlist();

    QVERIFY(playlist->createdOn() == databasePlaylist->playlistCreatedOn());
    QVERIFY(playlist->creatorName() == databasePlaylist->playlistCreator());
    QVERIFY(playlist->guid() == databasePlaylist->playlistGuid());
    QVERIFY(playlist->currentRevisionGuid() == databasePlaylist->currentRevisionGuid());
    QVERIFY(playlist->information() == databasePlaylist->playlistInformation());
    QVERIFY(playlist->isShared() == databasePlaylist->isShared());
    QVERIFY(playlist->title() == databasePlaylist->playlistTitle());
}
