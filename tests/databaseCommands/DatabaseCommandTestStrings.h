/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef DATABASECOMMANDTESTSTRINGS_H
#define DATABASECOMMANDTESTSTRINGS_H

#include "GlobalUtilityMacros.h"

#include <QByteArray>
#include <QList>

namespace ProtocolStr
{

struct DatabaseCommandTestStrings
{
    static const QList<QByteArray> addFiles();
    static const QList<QByteArray> deleteFiles();
    static const QList<QByteArray> createPlaylist();
    static const QList<QByteArray> renamePlaylist();
    static const QList<QByteArray> setPlaylistRevision();
    static const QList<QByteArray> deletePlaylist();
    static const QList<QByteArray> socialAction();
    static const QList<QByteArray> logPlayback();

    Q_DISABLE_INSTANTIATION(DatabaseCommandTestStrings);
};

}

#endif // DATABASECOMMANDTESTSTRINGS_H
