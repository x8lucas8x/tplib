/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "DatabaseCommandCommomTests.h"

#include "DatabaseCommandUtilityFunctions.h"
#include "DatabaseCommand.h"
#include "MsgProcessingUtilityFunctions.h"

#include <QVariantMap>
#include <QDebug>
#include <QTest>

void
CommandsCommomTests::testIfCommandIsInitialized( const DatabaseCommandPtr &command,
                                                 const DatabaseCommand::DatabaseCommandType &commandType )
{
    QVERIFY(!command.isNull());
    QVERIFY(command->type() == commandType);
}

void
CommandsCommomTests::testIfCommandTypeIsOk( const DatabaseCommandPtr &command,
                                            const QList<QByteArray> &jsons )
{
    foreach( const QByteArray &jsonIn, jsons )
    {
        QVariant parsedJsonIn = MsgProcessingUtils::parseJson( jsonIn );
        bool rightCommand = command->jsonToDatabaseCommand( parsedJsonIn.toMap() );
        QVERIFY(rightCommand == true);
    }
}

void
CommandsCommomTests::testIfDatabaseCommandProducesValidJson( const DatabaseCommandPtr &command,
                                                             const QList<QByteArray> &jsons )
{
    foreach( const QByteArray &jsonIn, jsons )
    {
        QVariant parsedJsonIn = MsgProcessingUtils::parseJson( jsonIn );
        command->jsonToDatabaseCommand( parsedJsonIn.toMap() );
        const QByteArray &jsonOut = MsgProcessingUtils::serializeJson( command->databaseCommandToJson() );
        QVERIFY(MsgProcessingUtils::isValidJson( jsonOut ) == true);
    }
}

void
CommandsCommomTests::testJsonToDatabaseCommandConversion( const DatabaseCommandPtr &command,
                                                          const QList<QByteArray> &jsons )
{
    foreach( const QByteArray &jsonIn, jsons )
    {
        qDebug() << "IN: " << jsonIn;

        QVariant parsedJsonIn = MsgProcessingUtils::parseJson( jsonIn );

        // Test two times
        command->jsonToDatabaseCommand( parsedJsonIn.toMap() );
        command->jsonToDatabaseCommand( parsedJsonIn.toMap() );

        // Test two times
        const QByteArray &jsonOut = MsgProcessingUtils::serializeJson( command->databaseCommandToJson() );
        const QByteArray &jsonOutBackup = MsgProcessingUtils::serializeJson( command->databaseCommandToJson() );

        QVERIFY(jsonOut == jsonOutBackup);

        qDebug() << "OUT: " <<  jsonOut;

        QVariant parsedJsonOut = MsgProcessingUtils::parseJson( jsonOut );
        QVERIFY(parsedJsonIn.toMap() == parsedJsonOut.toMap());
    }
}
