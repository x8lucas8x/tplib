/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "AddFilesCommandTest.h"

#include "AddFilesCommand.h"
#include "DatabaseCommandCommomTests.h"
#include "DatabaseCommandTestStrings.h"
#include "Track.h"
#include "MsgProcessingUtilityFunctions.h"

AddFilesCommandTest::AddFilesCommandTest( QObject *parent )
    : QObject( parent )
    , m_command( AddFilesCommand::create() )
    , m_jsons( ProtocolStr::DatabaseCommandTestStrings::addFiles() )
{
}

void
AddFilesCommandTest::init()
{
    CommandsCommomTests::testIfCommandIsInitialized( m_command,
                                                     DatabaseCommand::AddFilesCommand );
}

void
AddFilesCommandTest::testIfCommandTypeIsOk()
{
    CommandsCommomTests::testIfCommandTypeIsOk( m_command, m_jsons );
}

void
AddFilesCommandTest::testIfDatabaseCommandProducesValidJson()
{
    CommandsCommomTests::testIfDatabaseCommandProducesValidJson( m_command,
                                                                 m_jsons );
}

void
AddFilesCommandTest::testJsonToAddFilesCommandConversion()
{
    CommandsCommomTests::testJsonToDatabaseCommandConversion( m_command,
                                                              m_jsons );
}

void
AddFilesCommandTest::testDatabaseFileToTrackConversion()
{
    QVariant parsedJsonIn = MsgProcessingUtils::parseJson( m_jsons.first() );
    m_command->jsonToDatabaseCommand( parsedJsonIn.toMap() );

    TPCoreApi::TrackPtr track;
    TrackPtr databaseTrack;

    foreach( const TrackPtr &tempTrack, m_command->tracks() )
    {
        databaseTrack = tempTrack;
        track = tempTrack->toTrack();
        databaseTrack->fromTrack( track );

        QVERIFY(tempTrack->albumName() == databaseTrack->albumName());
        QVERIFY(tempTrack->albumPosition() == databaseTrack->albumPosition());
        QVERIFY(tempTrack->artistName() == databaseTrack->artistName());
        QVERIFY(tempTrack->mimeType() == databaseTrack->mimeType());
        QVERIFY(tempTrack->trackBitRate() == databaseTrack->trackBitRate());
        QVERIFY(tempTrack->trackDuration() == databaseTrack->trackDuration());
        QVERIFY(tempTrack->trackHash() == databaseTrack->trackHash());
        QVERIFY(tempTrack->trackId() == databaseTrack->trackId());
        QVERIFY(tempTrack->trackModifiedTime() == databaseTrack->trackModifiedTime());
        QVERIFY(tempTrack->trackName() == databaseTrack->trackName());
        QVERIFY(tempTrack->trackSize() == databaseTrack->trackSize());
        QVERIFY(tempTrack->trackUrl() == databaseTrack->trackUrl());
        QVERIFY(tempTrack->trackYear() == databaseTrack->trackYear());
    }
}
