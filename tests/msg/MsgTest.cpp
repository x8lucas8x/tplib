/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "MsgTest.h"

#include "Msg.h"

MsgTest::MsgTest()
{
}

void
MsgTest::testPayload()
{
    MsgPtr msg;
    QByteArray compressedPayload = QByteArray( Msg::maximumUncompressedMsgLength() + 1, 'a' );
    QByteArray uncompressedPayload = QByteArray( Msg::maximumUncompressedMsgLength() - 1, 'a' );

    msg = Msg::create( compressedPayload, Msg::JSON );
    QVERIFY(( msg->payloadLength() > 0 ) && ( msg->payloadLength() < compressedPayload.length() ));
    QVERIFY(msg->payload() == compressedPayload);
    QVERIFY(msg->hasFlag( Msg::COMPRESSED));

    msg = Msg::create( compressedPayload, Msg::JSON, false );
    QVERIFY(( msg->payloadLength() > 0 ) && ( msg->payloadLength() == compressedPayload.length() ));
    QVERIFY(msg->payload() == compressedPayload);

    msg = Msg::create( uncompressedPayload, Msg::JSON );
    QVERIFY(( msg->payloadLength() > 0 ) && ( msg->payloadLength() == uncompressedPayload.length() ));
    QVERIFY(msg->payload() == uncompressedPayload);
}

void
MsgTest::testHasFlag()
{
    MsgPtr msg = Msg::create( "a", Msg::JSON | Msg::FRAGMENT );
    QVERIFY(msg->hasFlag( Msg::JSON ));
    QVERIFY(msg->hasFlag( Msg::FRAGMENT ));
    QVERIFY(!msg->hasFlag( Msg::DBOP ));
    QVERIFY(!msg->hasFlag( Msg::RAW ));
    QVERIFY(!msg->hasFlag( Msg::COMPRESSED));
    QVERIFY(!msg->hasFlag( Msg::PING ));
    QVERIFY(!msg->hasFlag( Msg::SETUP ));
    QVERIFY(!msg->hasFlag( Msg::RESERVED_1));
}

void
MsgTest::testFlags()
{
    MsgPtr msg;

    msg = Msg::create( "a", Msg::JSON);
    QVERIFY(msg->flags() & Msg::JSON);
}
