project(TPLib)

cmake_minimum_required(VERSION 2.8)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules)

find_package(Qt4 COMPONENTS QtCore QtNetwork QtTest REQUIRED)
set(QT_DONT_USE_QTGUI TRUE)
INCLUDE(${QT_USE_FILE})
ADD_DEFINITIONS(${QT_DEFINITIONS})

find_package(QJSON REQUIRED)

# add a target to generate API documentation with Doxygen
find_package(Doxygen)
if(DOXYGEN_FOUND)
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
    add_custom_target(doc ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                         COMMENT "Generating API documentation with Doxygen." VERBATIM
)
endif(DOXYGEN_FOUND)

option(TPLIB_BUILD_TESTS "Build all unit tests." ON)

if(TPLIB_BUILD_TESTS)
    add_custom_target(test testRunner WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                         COMMENT "Executing all unit tests." VERBATIM )
endif(TPLIB_BUILD_TESTS)

set( TPLIB_VERSION_MAJOR "1" )
set( TPLIB_VERSION_MINOR "0" )
set( TPLIB_VERSION_PATCH "0" )
set( TPLIB_VERSION "${MYGPO_QT_VERSION_MAJOR}.${MYGPO_QT_VERSION_MINOR}.${MYGPO_QT_VERSION_PATCH}" )

add_definitions( -DTOMAHAWK_PROTOCOL_VERSION=4 )

# Set urls for every directory in src folder
set(CORE_API_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/coreApi)
set(UTILS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/utils)
set(MSG_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/msg)
set(SERVER_NODE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/serverNode)
set(DATABASE_COMMANDS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/databaseCommands)
set(PROTOCOL_STRINGS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/protocolStrings)

# Transverse all the project's folders
add_subdirectory(src)

add_library(
tpLib
$<TARGET_OBJECTS:coreApiLib>
$<TARGET_OBJECTS:msgLib>
$<TARGET_OBJECTS:serverNodeLib>
$<TARGET_OBJECTS:utilsLib>
$<TARGET_OBJECTS:databaseCommandsLib>
)

target_link_libraries(tpLib ${QJSON_LIBRARIES} ${QT_QTCORE_LIBRARY} ${QT_QTNETWORK_LIBRARY})

if(TPLIB_BUILD_TESTS)
    add_subdirectory(tests)
    add_executable(
    testRunner
    ${CMAKE_CURRENT_SOURCE_DIR}/tests/main.cpp
    $<TARGET_OBJECTS:testMsgLib>
    $<TARGET_OBJECTS:testDatabaseCommandsLib>
    $<TARGET_OBJECTS:testUtilsLib>
    $<TARGET_OBJECTS:coreApiLib>
    $<TARGET_OBJECTS:msgLib>
    $<TARGET_OBJECTS:serverNodeLib>
    $<TARGET_OBJECTS:utilsLib>
    $<TARGET_OBJECTS:databaseCommandsLib>
    )

    target_link_libraries(
    testRunner
    ${QT_QTCORE_LIBRARY}
    ${QT_QTTEST_LIBRARY}
    ${QT_QTNETWORK_LIBRARY}
    ${QJSON_LIBRARIES}
    )
endif(TPLIB_BUILD_TESTS)
