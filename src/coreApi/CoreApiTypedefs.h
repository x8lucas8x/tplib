/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef COREAPITYPEDEFS_H
#define COREAPITYPEDEFS_H

#include <QSharedPointer>

class QHostAddress;

class QIODevice;
typedef QSharedPointer<QIODevice> QIODevicePtr;

class QTcpSocket;
typedef QSharedPointer<QTcpSocket> QTcpSocketPtr;

namespace TPCoreApi
{

class Source;
typedef QSharedPointer<Source> SourcePtr;
class SourceImpl;
typedef QSharedPointer<SourceImpl> SourceImplPtr;
typedef QList<SourcePtr> SourcePtrList;

class SourceManager;
typedef QSharedPointer<SourceManager> SourceManagerPtr;
class SourceManagerImpl;
typedef QSharedPointer<SourceManagerImpl> SourceManagerImplPtr;

class Track;
typedef QSharedPointer<Track> TrackPtr;
class TrackImpl;
typedef QSharedPointer<TrackImpl> TrackImplPtr;
typedef QList<TrackPtr> TrackPtrList;

class PlaylistRevisionEntry;
typedef QSharedPointer<PlaylistRevisionEntry> PlaylistRevisionEntryPtr;
class PlaylistRevisionEntryImpl;
typedef QSharedPointer<PlaylistRevisionEntryImpl> PlaylistRevisionEntryImplPtr;
typedef QList<PlaylistRevisionEntryPtr> PlaylistRevisionEntryPtrList;

class PlaylistRevision;
typedef QSharedPointer<PlaylistRevision> PlaylistRevisionPtr;
class PlaylistRevisionImpl;
typedef QSharedPointer<PlaylistRevisionImpl> PlaylistRevisionImplPtr;
typedef QList<PlaylistRevisionPtr> PlaylistRevisionPtrList;

class Playlist;
typedef QSharedPointer<Playlist> PlaylistPtr;
class PlaylistImpl;
typedef QSharedPointer<PlaylistImpl> PlaylistImplPtr;
typedef QList<PlaylistPtr> PlaylistPtrList;

}

#endif // COREAPITYPEDEFS_H
