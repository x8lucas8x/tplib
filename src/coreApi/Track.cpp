/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "Track.h"

using namespace TPCoreApi;

//------------------------------------------------------------------------------
namespace TPCoreApi
{

class TrackImpl
{
public:
    TrackImpl()
    {
    }

    quint32 id;
    QString url;
    QString artistName;
    QString albumName;
    QString name;
    QString mimeType;
    QString hash;
    uint year;
    uint albumPosition;
    qulonglong modifiedTime;
    qulonglong duration;
    uint bitRate;
    qulonglong size;
};

}
//------------------------------------------------------------------------------

Track::Track()
    : m_impl( TrackImplPtr( new TrackImpl ) )
{
}

TrackPtr
Track::create()
{
    return TrackPtr( new Track );
}

quint32
Track::id() const
{
    return m_impl->id;
}

QString
Track::url() const
{
    return m_impl->url;
}

QString
Track::artistName() const
{
    return m_impl->artistName;
}

QString
Track::albumName() const
{
    return m_impl->albumName;
}

QString
Track::name() const
{
    return m_impl->name;
}

QString
Track::mimeType() const
{
    return m_impl->mimeType;
}

QString
Track::hash() const
{
    return m_impl->hash;
}

uint
Track::year() const
{
    return m_impl->year;
}

uint
Track::albumPosition() const
{
    return m_impl->albumPosition;
}

qulonglong
Track::modifiedTime() const
{
    return m_impl->modifiedTime;
}

qulonglong
Track::duration() const
{
    return m_impl->duration;
}

uint
Track::bitRate() const
{
    return m_impl->bitRate;
}

qulonglong
Track::size() const
{
    return m_impl->size;
}

void
Track::setId( const quint32 id )
{
    m_impl->id = id;
}

void
Track::setUrl( const QString &url )
{
    m_impl->url = url;
}

void
Track::setArtistName( const QString &artistName )
{
    m_impl->artistName = artistName;
}

void
Track::setAlbumName( const QString &albumName )
{
    m_impl->albumName = albumName;
}

void
Track::setName( const QString &trackName )
{
    m_impl->name = trackName;
}

void
Track::setMimeType( const QString &mimeType )
{
    m_impl->mimeType = mimeType;
}

void
Track::setHash( const QString &hash )
{
    m_impl->hash = hash;
}

void
Track::setYear( const uint year )
{
    m_impl->year = year;
}

void
Track::setAlbumPosition( const uint position )
{
    m_impl->albumPosition = position;
}

void
Track::setModifiedTime( const qulonglong modifiedTime )
{
    m_impl->modifiedTime = modifiedTime;
}

void
Track::setDuration( const qulonglong duration )
{
    m_impl->duration = duration;
}

void
Track::setBitRate( const uint bitRate )
{
    m_impl->bitRate = bitRate;
}

void
Track::setSize( const qulonglong size )
{
    m_impl->size = size;
}
