/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef TRACK_H
#define TRACK_H

#include "CoreApiTypedefs.h"

#include <QObject>
#include <QVariantMap>

namespace TPCoreApi
{

class Track
{
public:
    static TrackPtr create();

    quint32 id() const;
    QString url() const;
    QString artistName() const;
    QString albumName() const;
    QString name() const;
    QString mimeType() const;
    QString hash() const;
    uint year() const;
    uint albumPosition() const;
    qulonglong modifiedTime() const;
    qulonglong duration() const;
    uint bitRate() const;
    qulonglong size() const;

    void setId( const quint32 id );
    void setUrl( const QString &url );
    void setArtistName( const QString &name );
    void setAlbumName( const QString &name );
    void setName( const QString &name );
    void setMimeType( const QString &mimeType );
    void setHash( const QString &hash );
    void setYear( const uint year );
    void setAlbumPosition( const uint position );
    void setModifiedTime( const qulonglong modifiedTime );
    void setDuration( const qulonglong duration );
    void setBitRate( const uint bitRate );
    void setSize( const qulonglong size );

private:
    explicit Track();

    TrackImplPtr m_impl;
};

}

#endif //TRACK_H
