/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef PLAYLIST_H
#define PLAYLIST_H

#include "CoreApiTypedefs.h"

#include <QObject>

namespace TPCoreApi
{

class Playlist
{
public:
    static PlaylistPtr create();

    const QString &guid() const;
    const QString &information() const;
    const QString &creatorName() const;
    const QString &title() const;
    qulonglong createdOn() const;
    bool isShared() const;
    const QString &currentRevisionGuid() const;

    void setGuid( const QString &guid );
    void setInformation( const QString &information );
    void setCreatorName( const QString &creatorName );
    void setTitle( const QString &playlistTitle );
    void setCreatedOn( const qulonglong &createdOn );
    void setIsShared( const bool &isShared );
    void setCurrentRevisionGuid( const QString &guid );

private:
    explicit Playlist();

    PlaylistImplPtr m_impl;
};
//------------------------------------------------------------------------------
class PlaylistRevision
{
public:
    static PlaylistRevisionPtr create();

    const QString &revisionGuid() const;
    const PlaylistRevisionEntryPtrList &addedEntries() const;
    const QList<QString> &orderedEntryGuids() const;

    void setRevisionGuid( const QString &guid );
    void setAddedEntries( const PlaylistRevisionEntryPtrList &entries );
    void setOrderedEntryGuids( const QList<QString> &guids );

private:
    explicit PlaylistRevision();

    PlaylistRevisionImplPtr m_impl;
};
//------------------------------------------------------------------------------
class PlaylistRevisionEntry
{
public:
    static PlaylistRevisionEntryPtr create();

    const QString &entryGuid() const;
    qulonglong lastModified() const;
    const QString &trackName() const;
    const QString &artistName() const;
    const QString &albumName() const;
    qulonglong trackDuration() const;

    void setEntryGuid( const QString &guid );
    void setLastModified( const qulonglong lastModified );
    void setTrackName( const QString &name );
    void setArtistName( const QString &name );
    void setAlbumName( const QString &name );
    void setTrackDuration( const qulonglong duration );

private:
    explicit PlaylistRevisionEntry();

    PlaylistRevisionEntryImplPtr m_impl;
};

}

#endif // PLAYLIST_H
