/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef SOURCE_H
#define SOURCE_H

#include "CoreApiTypedefs.h"
#include "ServerNodeTypedefs.h"

#include <QObject>

namespace TPCoreApi
{

class Source : public QObject
{
    Q_OBJECT
public:
    static SourcePtr create( const QTcpSocketPtr &socket );
    
    const QHostAddress &peerAddress();
    const QIODevicePtr &resolveRemoteTrack( const quint32 &trackId );

    bool addNewConnection( const QTcpSocketPtr &socket );

signals:
    void shutdown();

    void tracksAdded( const TPCoreApi::TrackPtrList tracks );
    void tracksRemoved( const QList<qulonglong> trackIds );
    void playlistsCreated( const TPCoreApi::PlaylistPtrList playlists );
    void playlistsDeleted( const QList<QString> playlistIds );
    void playlistChanged( const QString playlistId, const TPCoreApi::PlaylistRevisionPtr revision );
    void playlistRenamed( const QString playlistId, const QString &newTitle );
    void playbackStarted( const QString &artistName, const QString &trackName );
    void playbackFinished( const QString &artistName, const QString &trackName, const quint32 secondsPlayed );
    void trackLoved( const QString &artistName, const QString &trackName );
    void trackUnloved( const QString &artistName, const QString &trackName );

private slots:
    void onCreateControlConnectionServerNode( ControlConnectionServerNodePtr serverNode );
    void onCreateDatabaseSyncConnectionServerNode( DatabaseSyncConnectionServerNodePtr serverNode );
    void onRequestForOutboundStreamConnection( const QString &controlId,
                                               const quint32 &trackId,
                                               const QTcpSocketPtr &socket );
    void onStartDatabaseSyncing( const QString &requesterUUID, const QTcpSocketPtr &socket );

    void onShutdownControlConnectionServerNode();
    void onShutdownDatabaseSyncConnectionServerNode();
    void onShutdownInboundStreamConnectionServerNode();
    void onShutdownOutboundStreamConnectionServerNode();

private:
    explicit Source( const QTcpSocketPtr &socket, QObject *parent = 0 );

    SourceImplPtr m_impl;
};

}

#endif // SOURCE_H
