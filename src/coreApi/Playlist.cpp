/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "Playlist.h"

using namespace TPCoreApi;

//------------------------------------------------------------------------------
namespace TPCoreApi
{

class PlaylistImpl
{
public:
    PlaylistImpl()
    {
    }

    QString information;
    QString creatorName;
    QString title;
    qulonglong createdOn;
    bool isShared;
    QString currentRevisionGuid;
    QString guid;
};

}
//------------------------------------------------------------------------------

Playlist::Playlist()
    : m_impl( PlaylistImplPtr( new PlaylistImpl ) )
{
}

PlaylistPtr
Playlist::create()
{
    return PlaylistPtr( new Playlist );
}

const QString&
Playlist::information() const
{
    return m_impl->information;
}

const QString&
Playlist::creatorName() const
{
    return m_impl->creatorName;
}

const QString&
Playlist::title() const
{
    return m_impl->title;
}

qulonglong
Playlist::createdOn() const
{
    return m_impl->createdOn;
}

bool
Playlist::isShared() const
{
    return m_impl->isShared;
}

const QString&
Playlist::currentRevisionGuid() const
{
    return m_impl->currentRevisionGuid;
}

const QString&
Playlist::guid() const
{
    return m_impl->guid;
}

void
Playlist::setInformation( const QString &information )
{
    m_impl->information = information;
}

void
Playlist::setCreatorName( const QString &creatorName )
{
    m_impl->creatorName = creatorName;
}

void
Playlist::setTitle( const QString &playlistTitle )
{
    m_impl->title = playlistTitle;
}

void
Playlist::setCreatedOn( const qulonglong &createdOn )
{
    m_impl->createdOn = createdOn;
}

void
Playlist::setIsShared( const bool &isShared )
{
    m_impl->isShared = isShared;
}

void
Playlist::setCurrentRevisionGuid( const QString &guid )
{
    m_impl->currentRevisionGuid = guid;
}

void
Playlist::setGuid( const QString &guid )
{
    m_impl->guid = guid;
}

//------------------------------------------------------------------------------
namespace TPCoreApi
{

class PlaylistRevisionImpl
{
public:
    PlaylistRevisionImpl()
    {
    }

    QString revisionGuid;
    PlaylistRevisionEntryPtrList addedEntries;
    QList<QString> orderedEntryGuids;
};

}
//------------------------------------------------------------------------------

PlaylistRevision::PlaylistRevision()
    : m_impl( PlaylistRevisionImplPtr( new PlaylistRevisionImpl ) )

{
}

PlaylistRevisionPtr
PlaylistRevision::create()
{
    return PlaylistRevisionPtr( new PlaylistRevision );
}

const QString&
PlaylistRevision::revisionGuid() const
{
    return m_impl->revisionGuid;
}

const PlaylistRevisionEntryPtrList&
PlaylistRevision::addedEntries() const
{
    return m_impl->addedEntries;
}

const QList<QString>&
PlaylistRevision::orderedEntryGuids() const
{
    return m_impl->orderedEntryGuids;
}

void
PlaylistRevision::setRevisionGuid( const QString &guid )
{
    m_impl->revisionGuid = guid;
}

void
PlaylistRevision::setAddedEntries( const PlaylistRevisionEntryPtrList &entries )
{
    m_impl->addedEntries = entries;
}

void
PlaylistRevision::setOrderedEntryGuids( const QList<QString> &guids )
{
    m_impl->orderedEntryGuids = guids;
}

//------------------------------------------------------------------------------
namespace TPCoreApi
{

class PlaylistRevisionEntryImpl
{
public:
    PlaylistRevisionEntryImpl()
    {
    }

    QString entryGuid;
    qulonglong lastModified;
    QString trackName;
    QString artistName;
    QString albumName;
    qulonglong trackDuration;
};

}
//------------------------------------------------------------------------------

PlaylistRevisionEntry::PlaylistRevisionEntry()
    : m_impl( PlaylistRevisionEntryImplPtr( new PlaylistRevisionEntryImpl ) )
{
}

PlaylistRevisionEntryPtr
PlaylistRevisionEntry::create()
{
    return PlaylistRevisionEntryPtr( new PlaylistRevisionEntry );
}

const QString&
PlaylistRevisionEntry::entryGuid() const
{
    return m_impl->entryGuid;
}

qulonglong
PlaylistRevisionEntry::lastModified() const
{
    return m_impl->lastModified;
}

const QString&
PlaylistRevisionEntry::trackName() const
{
    return m_impl->trackName;
}

const QString&
PlaylistRevisionEntry::artistName() const
{
    return m_impl->artistName;
}

const QString&
PlaylistRevisionEntry::albumName() const
{
    return m_impl->albumName;
}

qulonglong
PlaylistRevisionEntry::trackDuration() const
{
    return m_impl->trackDuration;
}

void
PlaylistRevisionEntry::setEntryGuid( const QString &guid )
{
    m_impl->entryGuid;
}

void
PlaylistRevisionEntry::setLastModified( const qulonglong lastModified )
{
    m_impl->lastModified = lastModified;
}

void
PlaylistRevisionEntry::setTrackName( const QString &name )
{
    m_impl->trackName = name;
}

void
PlaylistRevisionEntry::setArtistName( const QString &name )
{
    m_impl->artistName = name;
}

void
PlaylistRevisionEntry::setAlbumName( const QString &name )
{
    m_impl->albumName = name;
}

void
PlaylistRevisionEntry::setTrackDuration( const qulonglong duration )
{
    m_impl->trackDuration = duration;
}
