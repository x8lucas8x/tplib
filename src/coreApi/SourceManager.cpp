/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "SourceManager.h"

#include "Source.h"

#include <QList>
#include <QTcpServer>
#include <QTcpSocket>

using namespace TPCoreApi;

//------------------------------------------------------------------------------
namespace TPCoreApi
{

class SourceManagerImpl
{
public:
    SourceManagerImpl()
    {

    }

    QTcpServer server;
    QHash<QHostAddress, SourcePtr> sources;
};

}
//------------------------------------------------------------------------------

SourceManager::SourceManager( QObject *parent )
    : QObject( parent )
    , m_impl( SourceManagerImplPtr( new SourceManagerImpl ) )
{
    connect( &m_impl->server, SIGNAL(newConnection()),
             this, SLOT(onNewConnection) );
}

void
SourceManager::onNewConnection()
{
    // Get the new socket
    QTcpSocket* newConnection = m_impl->server.nextPendingConnection();

    disconnect( newConnection, SIGNAL(readyRead()),
                &m_impl->server, SLOT(readyRead()) );
    disconnect( newConnection, SIGNAL( disconnected() ),
                newConnection, SLOT(deleteLater()) );
    disconnect( newConnection, SIGNAL(error(QAbstractSocket::SocketError)),
                &m_impl->server, SLOT(socketError(QAbstractSocket::SocketError)) );

    // Now, it's up to the Source the responsability to handle this socket and
    // its respective Connections
    QTcpSocketPtr socket = QTcpSocketPtr( newConnection );
    SourcePtr source;

    if( m_impl->sources.contains( socket->peerAddress() ) )
    {
        source = m_impl->sources.value( socket->peerAddress() );
        source->addNewConnection( socket );

    }
    else
    {
        source = Source::create( socket );
        m_impl->sources.insert( socket->peerAddress(), source );
    }

    connect( source.data(), SIGNAL(shutdown()),
             this, SLOT(onShutdownSource()) );

    emit newSource( source );
}

void
SourceManager::onShutdownSource()
{
    Source *source = dynamic_cast<Source *>( sender() );
    m_impl->sources.remove( source->peerAddress() );
}
