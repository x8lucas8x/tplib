/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "Source.h"

#include "ConnectionHandlerServerNode.h"
#include "ControlConnectionServerNode.h"
#include "DatabaseSyncConnectionServerNode.h"
#include "StreamConnectionServerNode.h"
#include "NetworkUtilityFunctions.h"
#include "Track.h"

#include <QHostAddress>
#include <QTcpSocket>
#include <QIODevice>

using namespace TPCoreApi;

//------------------------------------------------------------------------------
namespace TPCoreApi
{

class SourceImpl
{
public:
    SourceImpl( const QHostAddress &_peerAddress, const quint16 &_peerPort )
        : peerAddress( _peerAddress )
        , peerPort( _peerPort )
        , connectionHandlerServerNode( 0 )
        , controlConnectionServerNode( 0 )
        , databaseSyncConnectionServerNode( 0 )
        , inboundStreamConnectionServerNode( 0 )
    {
    }

    const QHostAddress peerAddress;
    const quint16 peerPort;
    ConnectionHandlerServerNodePtr connectionHandlerServerNode;
    ControlConnectionServerNodePtr controlConnectionServerNode;
    DatabaseSyncConnectionServerNodePtr databaseSyncConnectionServerNode;
    StreamConnectionServerNodePtr inboundStreamConnectionServerNode;
    QMap<QString, StreamConnectionServerNodePtr> outboundStreamConnectionServerNodes;
};

}
//------------------------------------------------------------------------------

Source::Source( const QTcpSocketPtr &socket, QObject *parent )
    : QObject( parent )
    , m_impl( SourceImplPtr( new SourceImpl( socket->peerAddress(), socket->peerPort() ) ) )
{
    addNewConnection( socket );
}


SourcePtr
Source::create( const QTcpSocketPtr &socket )
{
    return SourcePtr( new Source( socket ) );
}

const QHostAddress&
Source::peerAddress()
{
    return m_impl->peerAddress;
}

const QIODevicePtr&
Source::resolveRemoteTrack( const quint32 &trackId )
{
    QTcpSocketPtr socket = NetworkUtils::createTcpConnection( m_impl->peerAddress,
                                                              m_impl->peerPort );

    TPCoreApi::TrackPtr track;

    //TODO: Load the track from the database.

    StreamConnectionServerNodePtr serverNode;
    serverNode = StreamConnectionServerNode::create( m_impl->controlConnectionServerNode->id(),
                                                     track,
                                                     socket );
    connect( serverNode.data(), SIGNAL(shutdown()),
             this, SLOT(onShutdownInboundStreamConnectionServerNode()) );

    m_impl->inboundStreamConnectionServerNode = serverNode;
    return m_impl->inboundStreamConnectionServerNode->resolveTrackBuffer();
}

bool
Source::addNewConnection( const QTcpSocketPtr &socket )
{
    if( m_impl->peerAddress != socket->peerAddress() )
        return false;

    if( m_impl->controlConnectionServerNode &&
        m_impl->databaseSyncConnectionServerNode &&
        m_impl->inboundStreamConnectionServerNode )
        return false;

    m_impl->connectionHandlerServerNode = ConnectionHandlerServerNode::create( socket );

    connect( m_impl->connectionHandlerServerNode.data(),
             SIGNAL(createControlConnectionServerNode(ControlConnectionServerNodePtr)),
             this,
             SLOT(onCreateControlConnectionServerNode(ControlConnectionServerNodePtr)) );
    connect( m_impl->connectionHandlerServerNode.data(),
             SIGNAL(createDatabaseSyncConnectionServerNode(DatabaseSyncConnectionServerNodePtr)),
             this,
             SLOT(onCreateDatabaseSyncConnectionServerNode(DatabaseSyncConnectionServerNodePtr)) );
    connect( m_impl->connectionHandlerServerNode.data(),
             SIGNAL(requestForOutboundStreamConnection(QString,quint32,QTcpSocketPtr)),
             this,
             SLOT(onRequestForOutboundStreamConnection(QString,quint32,QTcpSocketPtr)) );
}

void
Source::onCreateControlConnectionServerNode( ControlConnectionServerNodePtr serverNode )
{
    if( !m_impl->controlConnectionServerNode.isNull() )
    {
        connect( serverNode.data(), SIGNAL(shutdown()),
                 this, SLOT(onShutdownControlConnectionServerNode()) );
        connect( serverNode.data(), SIGNAL(startDatabaseSyncing(QString,QTcpSocketPtr)),
                 this, SLOT(startDatabaseSyncing(QString,QTcpSocketPtr)) );

        m_impl->controlConnectionServerNode = serverNode;
    }
}

void
Source::onCreateDatabaseSyncConnectionServerNode( DatabaseSyncConnectionServerNodePtr serverNode )
{
    if( !m_impl->databaseSyncConnectionServerNode.isNull() )
    {
        connect( serverNode.data(), SIGNAL(shutdown()),
                 this, SLOT(onShutdownDatabaseSyncConnectionServerNode()) );
        connect( serverNode.data(), SIGNAL(tracksAdded(TPCoreApi::TrackPtrList)),
                 this, SIGNAL(tracksAdded(TPCoreApi::TrackPtrList)) );
        connect( serverNode.data(), SIGNAL(tracksRemoved(QList<qulonglong>)),
                 this, SIGNAL(tracksRemoved(QList<qulonglong>)) );
        connect( serverNode.data(), SIGNAL(playlistsCreated(TPCoreApi::PlaylistPtrList)),
                 this, SIGNAL(playlistsCreated(TPCoreApi::PlaylistPtrList)) );
        connect( serverNode.data(), SIGNAL(playlistsDeleted(QList<QString>)),
                 this, SIGNAL(playlistsDeleted(QList<QString>)) );
        connect( serverNode.data(), SIGNAL(playlistRenamed(QString,QString)),
                 this, SIGNAL(playlistRenamed(QString,QString)) );
        connect( serverNode.data(), SIGNAL(playlistChanged(QString,TPCoreApi::PlaylistRevisionPtr)),
                 this, SIGNAL(playlistChanged(QString,TPCoreApi::PlaylistRevisionPtr)) );
        connect( serverNode.data(), SIGNAL(playbackStarted(QString,QString)),
                 this, SIGNAL(playbackStarted(QString,QString)) );
        connect( serverNode.data(), SIGNAL(playbackFinished(QString,QString,quint32)),
                 this, SIGNAL(playbackFinished(QString,QString,quint32)) );
        connect( serverNode.data(), SIGNAL(trackLoved(QString,QString)),
                 this, SIGNAL(trackLoved(QString,QString)) );
        connect( serverNode.data(), SIGNAL(trackUnloved(QString,QString)),
                 this, SIGNAL(trackUnloved(QString,QString)) );

        m_impl->databaseSyncConnectionServerNode = serverNode;
    }
}

void
Source::onRequestForOutboundStreamConnection( const QString &controlId,
                                              const quint32 &trackId,
                                              const QTcpSocketPtr &socket )
{
    StreamConnectionServerNodePtr serverNode;

    TPCoreApi::TrackPtr track;

    //TODO: Load the track from the database.

    serverNode = StreamConnectionServerNode::create( controlId,
                                                     track,
                                                     socket );

    connect( serverNode.data(), SIGNAL(shutdown()),
             this, SLOT(onShutdownOutboundStreamConnectionServerNode()) );

    m_impl->outboundStreamConnectionServerNodes.insert( serverNode->id(), serverNode );
}

void
Source::onStartDatabaseSyncing( const QString &requesterUUID, const QTcpSocketPtr &socket )
{
    if( !m_impl->databaseSyncConnectionServerNode.isNull() )
        return;

    DatabaseSyncConnectionServerNodePtr serverNode;
    serverNode = DatabaseSyncConnectionServerNode::create( requesterUUID, socket );
    onCreateDatabaseSyncConnectionServerNode( serverNode );
}

void
Source::onShutdownControlConnectionServerNode()
{
    m_impl->controlConnectionServerNode.clear();
    m_impl->databaseSyncConnectionServerNode.clear();
    m_impl->inboundStreamConnectionServerNode.clear();
    m_impl->outboundStreamConnectionServerNodes.clear();

    emit shutdown();
}

void
Source::onShutdownDatabaseSyncConnectionServerNode()
{
    m_impl->databaseSyncConnectionServerNode.clear();
}

void
Source::onShutdownInboundStreamConnectionServerNode()
{
    m_impl->inboundStreamConnectionServerNode.clear();
}

void
Source::onShutdownOutboundStreamConnectionServerNode()
{
    StreamConnectionServerNode *senderNode = dynamic_cast<StreamConnectionServerNode *>( sender() );
    m_impl->outboundStreamConnectionServerNodes.remove( senderNode->id() );
}
