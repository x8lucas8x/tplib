/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef SERVERNODETYPEDEFS_H
#define SERVERNODETYPEDEFS_H

#include <QSharedPointer>

class QTcpSocket;
typedef QSharedPointer<QTcpSocket> QTcpSocketPtr;

class QTimer;
typedef QSharedPointer<QTimer> QTimerPtr;

class ServerNode;
typedef QSharedPointer<ServerNode> ServerNodePtr;
class ServerNodeImpl;
typedef QSharedPointer<ServerNodeImpl> ServerNodeImplPtr;

class ConnectionServerNode;
typedef QSharedPointer<ConnectionServerNode> ConnectionServerNodePtr;
class ConnectionServerNodeImpl;
typedef QSharedPointer<ConnectionServerNodeImpl> ConnectionServerNodeImplPtr;

class ConnectionHandlerServerNode;
typedef QSharedPointer<ConnectionHandlerServerNode> ConnectionHandlerServerNodePtr;
class ConnectionHandlerServerNodeImpl;
typedef QSharedPointer<ConnectionHandlerServerNodeImpl> ConnectionHandlerServerNodeImplPtr;

class ControlConnectionServerNode;
typedef QSharedPointer<ControlConnectionServerNode> ControlConnectionServerNodePtr;
class ControlConnectionServerNodeImpl;
typedef QSharedPointer<ControlConnectionServerNodeImpl> ControlConnectionServerNodeImplPtr;

class DatabaseSyncConnectionServerNode;
typedef QSharedPointer<DatabaseSyncConnectionServerNode> DatabaseSyncConnectionServerNodePtr;
class DatabaseSyncConnectionServerNodeImpl;
typedef QSharedPointer<DatabaseSyncConnectionServerNodeImpl> DatabaseSyncConnectionServerNodeImplPtr;

class StreamConnectionServerNode;
typedef QSharedPointer<StreamConnectionServerNode> StreamConnectionServerNodePtr;
class StreamConnectionServerNodeImpl;
typedef QSharedPointer<StreamConnectionServerNodeImpl> StreamConnectionServerNodeImplPtr;

#endif // SERVERNODETYPEDEFS_H
