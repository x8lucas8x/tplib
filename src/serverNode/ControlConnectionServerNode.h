/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef CONTROLCONNECTIONSERVERNODE_H
#define CONTROLCONNECTIONSERVERNODE_H

#include "MsgTypedefs.h"
#include "ServerNodeTypedefs.h"
#include "ConnectionServerNode.h"

#include <QObject>

class ControlConnectionServerNode : public ConnectionServerNode
{
    Q_OBJECT
public:
    static ControlConnectionServerNodePtr create( const QString &nodeId,
                                                  QTcpSocketPtr socket );

protected:
    virtual void processNewMessage( const MsgPtr &msg );
    virtual void init();

signals:
    void startDatabaseSyncing( const QString &requesterUUID, const QTcpSocketPtr &socket );

private slots:
    void onPingReceived();
    void onStartDatabaseSyncing( const QString &requesterUUID );

private:
    explicit ControlConnectionServerNode( const QString &nodeId,
                                          QTcpSocketPtr socket,
                                          QObject *parent = 0 );

    ControlConnectionServerNodeImplPtr m_impl;
};

#endif // CONTROLCONNECTIONSERVERNODE_H
