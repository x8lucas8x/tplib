/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "DatabaseSyncConnectionServerNode.h"

#include "DatabaseSyncConnectionMsgProcessor.h"
#include "DatabaseSyncConnectionMsgFactory.h"
#include "DatabaseCommand.h"

#include "Debug.h"
#define DEBUG_FULL

//------------------------------------------------------------------------------
class DatabaseSyncConnectionServerNodeImpl
{
public:
    DatabaseSyncConnectionServerNodeImpl( const QString &_lastOperationGuid )
        : messageProcessor( DatabaseSyncConnectionMsgProcessor::create() )
        , lastOperatioGuid( _lastOperationGuid )
    {
    }

    DatabaseSyncConnectionMsgProcessorPtr messageProcessor;
    QString lastOperatioGuid;
};
//------------------------------------------------------------------------------

DatabaseSyncConnectionServerNode::DatabaseSyncConnectionServerNode( const QString &controlId,
                                                                    QTcpSocketPtr socket,
                                                                    const QString &lastOperationGuid,
                                                                    QObject *parent )
    : ConnectionServerNode( controlId, socket, parent )
    , m_impl( DatabaseSyncConnectionServerNodeImplPtr( new DatabaseSyncConnectionServerNodeImpl( lastOperationGuid ) ) )
{
    // The rest of this very message processor signals must be connected into
    // init() function instead.
    connect( m_impl->messageProcessor.data(), SIGNAL(setupAndVersionCheckSucceed()),
             this, SLOT(onSetupAndVersionCheckSucceed()) );
    connect( m_impl->messageProcessor.data(), SIGNAL(setupAndVersionCheckFailed()),
             this, SLOT(onSetupAndVersionCheckFailed()) );
}

DatabaseSyncConnectionServerNodePtr
DatabaseSyncConnectionServerNode::create( const QString &controlId,
                                          QTcpSocketPtr socket,
                                          const QString &lastOperationGuid  )
{
    return DatabaseSyncConnectionServerNodePtr( new DatabaseSyncConnectionServerNode( controlId,
                                                                                      socket,
                                                                                      lastOperationGuid ) );
}

void
DatabaseSyncConnectionServerNode::init()
{
    connect( m_impl->messageProcessor.data(), SIGNAL(fetchDatabaseOperations(QString)),
             this, SIGNAL(fetchDatabaseOperations(QString)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(triggerDatabaseOperationFetches()),
             this, SLOT(onTriggerDatabaseOperationFetches()) );

    connect( m_impl->messageProcessor.data(), SIGNAL(tracksAdded(TPCoreApi::TrackPtrList)),
             this, SIGNAL(tracksAdded(TPCoreApi::TrackPtrList)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(tracksRemoved(QList<qulonglong>)),
             this, SIGNAL(tracksRemoved(QList<qulonglong>)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(playlistsCreated(TPCoreApi::PlaylistPtrList)),
             this, SIGNAL(playlistsCreated(TPCoreApi::PlaylistPtrList)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(playlistsDeleted(QList<QString>)),
             this, SIGNAL(playlistsDeleted(QList<QString>)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(playlistRenamed(QString,QString)),
             this, SIGNAL(playlistRenamed(QString,QString)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(playlistChanged(QString,TPCoreApi::PlaylistRevisionPtr)),
             this, SIGNAL(playlistChanged(QString,TPCoreApi::PlaylistRevisionPtr)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(playbackStarted(QString,QString)),
             this, SIGNAL(playbackStarted(QString,QString)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(playbackFinished(QString,QString,quint32)),
             this, SIGNAL(playbackFinished(QString,QString,quint32)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(trackLoved(QString,QString)),
             this, SIGNAL(trackLoved(QString,QString)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(trackUnloved(QString,QString)),
             this, SIGNAL(trackUnloved(QString,QString)) );
}

void
DatabaseSyncConnectionServerNode::processNewMessage( const MsgPtr &msg )
{
    m_impl->messageProcessor->startMessageProcessing( msg );
}


void
DatabaseSyncConnectionServerNode::onDatabaseSynced( const QString lastDatabaseOperationGuid )
{
    m_impl->lastOperatioGuid = lastDatabaseOperationGuid;
}

void
DatabaseSyncConnectionServerNode::onTriggerDatabaseOperationFetches()
{
    sendMessage( DatabaseSyncConnectionMsgFactory::createFetchDatabaseOperationsMessage( m_impl->lastOperatioGuid ) );
}
