/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef CONNECTIONHANDLERSERVERNODE_H
#define CONNECTIONHANDLERSERVERNODE_H

#include "MsgTypedefs.h"
#include "ServerNodeTypedefs.h"
#include "ServerNode.h"

#include <QObject>

class ConnectionHandlerServerNode : public ServerNode
{
    Q_OBJECT
public:
    static ConnectionHandlerServerNodePtr create( const QTcpSocketPtr &socket );

protected:
    virtual void processNewMessage( const MsgPtr &msg );

signals:
    void createControlConnectionServerNode( ControlConnectionServerNodePtr serverNode );
    void createDatabaseSyncConnectionServerNode( DatabaseSyncConnectionServerNodePtr serverNode );
    void requestForOutboundStreamConnection( const QString &controlId,
                                             const quint32 &trackId,
                                             const QTcpSocketPtr &socket );

private slots:
    void onRequestControlConnectionCreation( const QString &key,
                                             const QString &nodeId );
    void onRequestSecondaryConnectionCreation( const QString &key,
                                               const QString &controlId );
    void onReceivedRequestForReverseConnection( const QString &key,
                                                const QString &controlId,
                                                const QString &offerKey );

private:
    explicit ConnectionHandlerServerNode( QTcpSocketPtr socket,
                                          QObject *parent = 0 );

    bool isKeyWhitelist( const QString &key ) const;
    bool isKeyFileRequest( const QString &key ) const;
    bool isKeyUuid( const QString &key ) const;

    ConnectionHandlerServerNodeImplPtr m_impl;
};

#endif // CONNECTIONHANDLERSERVERNODE_H
