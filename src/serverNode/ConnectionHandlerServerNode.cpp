/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "ConnectionHandlerServerNode.h"

#include "ConnectionHandlerMsgProcessor.h"
#include "ConnectionHandlerMsgFactory.h"
#include "StreamConnectionServerNode.h"
#include "ControlConnectionServerNode.h"
#include "DatabaseSyncConnectionServerNode.h"
#include "NetworkUtilityFunctions.h"

#include <QHostAddress>
#include <QTcpSocket>
#include <QString>

#include "Debug.h"
#define DEBUG_FULL

//------------------------------------------------------------------------------
class ConnectionHandlerServerNodeImpl
{
public:
    ConnectionHandlerServerNodeImpl()
        : messageProcessor( ConnectionHandlerMsgProcessor::create() )
    {
    }

    ConnectionHandlerMsgProcessorPtr messageProcessor;
};
//------------------------------------------------------------------------------

//Key types
const QString whitelistKey = "whitelist";
const QString fileRequestKey = "FILE_REQUEST_KEY:";

ConnectionHandlerServerNode::ConnectionHandlerServerNode( QTcpSocketPtr socket,
                                                          QObject *parent )
    : ServerNode( socket, parent )
    , m_impl( ConnectionHandlerServerNodeImplPtr( new ConnectionHandlerServerNodeImpl ) )
{
    connect( m_impl->messageProcessor.data(),
             SIGNAL(requestControlConnectionCreation(QString,QString)),
             this, SLOT(onRequestControlConnectionCreation(QString,QString)));
    connect( m_impl->messageProcessor.data(),
             SIGNAL(requestSecondaryConnectionCreation(QString,QString)),
             this, SLOT(onRequestSecondaryConnectionCreation(QString,QString)));
    connect( m_impl->messageProcessor.data(),
             SIGNAL(receivedRequestForReverseConnection(QString,QString,QString)),
             this, SLOT(onReceivedRequestForReverseConnection(QString,QString,QString)));
}

bool
ConnectionHandlerServerNode::isKeyWhitelist( const QString &key ) const
{
    return key == whitelistKey;
}

bool
ConnectionHandlerServerNode::isKeyFileRequest( const QString &key ) const
{
    return key.startsWith( fileRequestKey );
}

bool
ConnectionHandlerServerNode::isKeyUuid( const QString &key ) const
{
    return !isKeyFileRequest( key ) && !isKeyWhitelist( key );
}

ConnectionHandlerServerNodePtr
ConnectionHandlerServerNode::create( const QTcpSocketPtr &socket )
{
    return ConnectionHandlerServerNodePtr( new ConnectionHandlerServerNode( socket ) );
}

void
ConnectionHandlerServerNode::processNewMessage( const MsgPtr &msg )
{
    m_impl->messageProcessor->startMessageProcessing( msg );
}

void
ConnectionHandlerServerNode::onRequestControlConnectionCreation( const QString &key,
                                                                 const QString &nodeId )
{
    DEBUG_BLOCK

    if( isKeyWhitelist( key ) ) // LAN IP address, check source IP
    {
        if( NetworkUtils::isIPWhitelisted( socket()->peerAddress() ) )
        {
            debug() << "Connection is from whitelisted IP range (LAN).";
            emit createControlConnectionServerNode( ControlConnectionServerNode::create( nodeId, socket() ) );
        }
        else
        {
            debug() << "Connection claimed to be whitelisted, but wasn't.";
        }
    }
}

void
ConnectionHandlerServerNode::onRequestSecondaryConnectionCreation( const QString &key,
                                                                   const QString &controlId )
{
    DEBUG_BLOCK

    // Magic key for stream connections.
    if( isKeyFileRequest( key ) )
    {
        bool hasAControlConnection = false;

        // check if the source IP matches an existing, authenticated connection
        if ( ( socket()->peerAddress() != QHostAddress::Any ) && !NetworkUtils::isIPWhitelisted( socket()->peerAddress() ) )
        {
            const QString fileId = key.right( key.length() - fileRequestKey.length() );
            emit requestForOutboundStreamConnection( controlId, fileId.toUInt(), socket() );
        }
    }
    else
    {
        emit createDatabaseSyncConnectionServerNode( DatabaseSyncConnectionServerNode::create( controlId, socket() ) );
    }
}

void
ConnectionHandlerServerNode::onReceivedRequestForReverseConnection( const QString &key,
                                                                    const QString &controlId,
                                                                    const QString &offerKey )
{
    DEBUG_BLOCK

    QTcpSocketPtr socket;
    const QString port = QString::number( socket->peerPort() );

    // Magic key for stream connections.
    if( isKeyFileRequest( offerKey ) )
    {
        socket = NetworkUtils::createTcpConnection( socket->peerAddress(), socket->peerPort() );
        const QString fileId = key.right( key.length() - fileRequestKey.length() );
        emit requestForOutboundStreamConnection( controlId, fileId.toUInt(), socket );

        sendMessage( ConnectionHandlerMsgFactory::createRequestConnectionResponse( controlId, key, port ) );
    }
    else
    {
        socket = NetworkUtils::createTcpConnection( socket->peerAddress(), socket->peerPort() );
        emit createDatabaseSyncConnectionServerNode( DatabaseSyncConnectionServerNode::create( controlId, socket ) );

        sendMessage( ConnectionHandlerMsgFactory::createRequestConnectionResponse( controlId, key, port ) );
    }
}
