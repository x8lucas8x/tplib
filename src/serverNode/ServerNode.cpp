/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "ServerNode.h"

#include "Msg.h"

#include <QtEndian>
#include <QTcpSocket>

#include "Debug.h"
#define DEBUG_FULL

//------------------------------------------------------------------------------
class ServerNodeImpl
{
public:
    ServerNodeImpl( QTcpSocketPtr _socket )
        : socket( _socket )
    {
    }

    QTcpSocketPtr socket;
};
//------------------------------------------------------------------------------

ServerNode::ServerNode( QTcpSocketPtr socket,
                        QObject *parent )
    : QObject( parent )
    , m_impl( ServerNodeImplPtr( new ServerNodeImpl( socket ) ) )
{
    connect( m_impl->socket.data(), SIGNAL(readyRead()),
             this, SLOT(onReadyRead()) );
    connect( m_impl->socket.data(), SIGNAL(disconnected()),
             this, SIGNAL(shutdown()));
    connect( m_impl->socket.data(), SIGNAL(disconnected()),
             m_impl->socket.data(), SLOT(deleteLater()) );
}

void
ServerNode::sendMessage( const MsgPtr &msg )
{
    DEBUG_BLOCK

    if ( m_impl->socket.isNull() || !m_impl->socket->isOpen() || !m_impl->socket->isWritable() )
    {
        debug() << "Socket problem, whilst in sendMsg(). Connection must shutdown.";
        emit shutdown();
    }
    else
    {
        const QByteArray data = msg->serialize();
        if ( m_impl->socket->write( data ) != data.length() )
        {
            debug() << "Error while writing to socket. Connection must shutdown.";
            emit shutdown();
        }
    }
}

QTcpSocketPtr
ServerNode::socket() const
{
    return m_impl->socket;
}

void
ServerNode::onReadyRead()
{
    DEBUG_BLOCK

    if ( m_impl->socket->bytesAvailable() < Msg::headerLength() )
        return;

    char msgHeader[Msg::headerLength()];

    if ( m_impl->socket->read( msgHeader, Msg::lengthFieldLength() ) != Msg::lengthFieldLength() )
    {
        debug() << tr( "Failed reading a msg header." );
        return;
    }

    quint32 length = qFromBigEndian( *( reinterpret_cast<quint32*>( msgHeader ) ) );
    quint8 flags = *( reinterpret_cast<quint8*>( msgHeader + sizeof(length) ) );

    if ( m_impl->socket->bytesAvailable() < length )
        return;

    QByteArray payload = m_impl->socket->read( length );
    if ( payload.length() != length )
    {
        debug() << tr( "Failed reading a message payload." );
        return;
    }

    processNewMessage( Msg::create( payload, flags, false ) );
}
