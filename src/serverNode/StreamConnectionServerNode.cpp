/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "StreamConnectionServerNode.h"

#include "StreamConnectionMsgProcessor.h"
#include "BufferIODevice.h"
#include "Track.h"

#include "Debug.h"
#define DEBUG_FULL

//------------------------------------------------------------------------------
class StreamConnectionServerNodeImpl
{
public:
    StreamConnectionServerNodeImpl( const TPCoreApi::TrackPtr _track )
        : messageProcessor( StreamConnectionMsgProcessor::create() )
        , track( _track )
    {
    }

    StreamConnectionMsgProcessorPtr messageProcessor;
    QIODevicePtr trackBuffer;
    const TPCoreApi::TrackPtr track;
};
//------------------------------------------------------------------------------

StreamConnectionServerNode::StreamConnectionServerNode( const QString &controlId,
                                                        const TPCoreApi::TrackPtr track,
                                                        QTcpSocketPtr socket,
                                                        QObject *parent )
    : ConnectionServerNode( controlId, socket, parent )
    , m_impl( StreamConnectionServerNodeImplPtr( new StreamConnectionServerNodeImpl( track ) ) )
{
    // The rest of this very message processor signals must be connected into
    // init() function instead.
    connect( m_impl->messageProcessor.data(), SIGNAL(setupAndVersionCheckSucceed()),
             this, SLOT(onSetupAndVersionCheckSucceed()) );
    connect( m_impl->messageProcessor.data(), SIGNAL(setupAndVersionCheckFailed()),
             this, SLOT(onSetupAndVersionCheckFailed()) );
    connect( m_impl->messageProcessor.data(), SIGNAL(seekClientBufferToBlock(quint32)),
             this, SIGNAL(seekClientBufferToBlock(quint32)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(seekServerBufferToBlock(quint32)),
             this, SIGNAL(seekServerBufferToBlock(quint32)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(newDataAvailable(QByteArray)),
             this, SIGNAL(onNewDataAvailable(QByteArray)) );
    connect( m_impl->messageProcessor.data(), SIGNAL(lastRawMessageReceivedInBatch(QByteArray)),
             this, SIGNAL(onLastRawMessageReceivedInBatch(QByteArray)) );

    BufferIODevice* buffer = new BufferIODevice( m_impl->track->size() );
    m_impl->trackBuffer = QIODevicePtr( buffer, &QObject::deleteLater );
    m_impl->trackBuffer->open( QIODevice::ReadWrite );

    // if the audioengine closes the iodev (skip/stop/etc) then kill the connection
    // immediately to avoid unnecessary network transfer.
    connect( m_impl->trackBuffer.data(), SIGNAL(aboutToClose()),
             this, SIGNAL(shutdown()), Qt::QueuedConnection );
    connect( m_impl->trackBuffer.data(), SIGNAL(blockRequest(int)),
             this, SLOT(onBlockRequest(int)) );
}

StreamConnectionServerNodePtr
StreamConnectionServerNode::create( const QString &controlId,
                                    const TPCoreApi::TrackPtr track,
                                    QTcpSocketPtr socket )
{
    return StreamConnectionServerNodePtr( new StreamConnectionServerNode( controlId, track, socket ) );
}

void
StreamConnectionServerNode::init()
{
}

void
StreamConnectionServerNode::processNewMessage( const MsgPtr &msg )
{
    m_impl->messageProcessor->startMessageProcessing( msg );
}

const QIODevicePtr&
StreamConnectionServerNode::resolveTrackBuffer()
{
    return m_impl->trackBuffer;
}
