/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef CONNECTIONSERVERNODE_H
#define CONNECTIONSERVERNODE_H

#include "ServerNodeTypedefs.h"
#include "ServerNode.h"

#include <QObject>

class ConnectionServerNode : public ServerNode
{
    Q_OBJECT
public:
    explicit ConnectionServerNode( const QString &id,
                                   QTcpSocketPtr socket,
                                   QObject *parent = 0 );

    const QString &id() const;

protected slots:
    void onSetupAndVersionCheckSucceed();
    void onSetupAndVersionCheckFailed();

protected:
    virtual void init() = 0;

private:
    ConnectionServerNodeImplPtr m_impl;
};

#endif // CONNECTIONSERVERNODE_H
