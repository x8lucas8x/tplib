/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "ControlConnectionServerNode.h"

#include "ControlConnectionMsgProcessor.h"

#include <QTimer>

#include "Debug.h"
#define DEBUG_FULL

//------------------------------------------------------------------------------
class ControlConnectionServerNodeImpl
{
public:
    ControlConnectionServerNodeImpl()
        : messageProcessor( ControlConnectionMsgProcessor::create() )
        , pingTimeout( QTimerPtr( new QTimer ) )
    {
        // If a ping message is not received within 10 minutes, then the
        // connection is closed.
        pingTimeout->setInterval( 600000 );
        pingTimeout->start();
    }

    ControlConnectionMsgProcessorPtr messageProcessor;
    QTimerPtr pingTimeout;
};
//------------------------------------------------------------------------------

ControlConnectionServerNode::ControlConnectionServerNode( const QString &nodeId,
                                                          QTcpSocketPtr socket,
                                                          QObject *parent )
    : ConnectionServerNode( nodeId, socket, parent )
    , m_impl( ControlConnectionServerNodeImplPtr( new ControlConnectionServerNodeImpl ) )
{
    // The rest of this very message processor signals must be connected into
    // init() function instead.
    connect( m_impl->messageProcessor.data(), SIGNAL(setupAndVersionCheckSucceed()),
             this, SLOT(onSetupAndVersionCheckSucceed()) );
    connect( m_impl->messageProcessor.data(), SIGNAL(setupAndVersionCheckFailed()),
             this, SLOT(onSetupAndVersionCheckFailed()) );
    connect( m_impl->pingTimeout.data(), SIGNAL(timeout()),
             this, SIGNAL(shutdown()) );
    connect( m_impl->messageProcessor.data(), SIGNAL(pingReceived()),
             this, SLOT(onPingReceived()) );
    connect( m_impl->messageProcessor.data(), SIGNAL(startDatabaseSyncing(QString)),
             this, SIGNAL(onStartDatabaseSyncing(QString)) );
}

ControlConnectionServerNodePtr
ControlConnectionServerNode::create( const QString &nodeId,
                                     QTcpSocketPtr socket )
{
    return ControlConnectionServerNodePtr( new ControlConnectionServerNode( nodeId, socket ) );
}

void
ControlConnectionServerNode::init()
{
}

void
ControlConnectionServerNode::processNewMessage( const MsgPtr &msg )
{
    m_impl->messageProcessor->startMessageProcessing( msg );
}

void
ControlConnectionServerNode::onPingReceived()
{
    m_impl->pingTimeout->start();
}

void
ControlConnectionServerNode::onStartDatabaseSyncing( const QString &requesterUUID )
{
    emit startDatabaseSyncing( requesterUUID, socket() );
}
