/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef DATABASESYNCCONNECTIONSERVERNODE_H
#define DATABASESYNCCONNECTIONSERVERNODE_H

#include "MsgTypedefs.h"
#include "ServerNodeTypedefs.h"
#include "DatabaseCommandsTypedefs.h"
#include "ConnectionServerNode.h"
#include "CoreApiTypedefs.h"

#include <QObject>

class DatabaseSyncConnectionServerNode : public ConnectionServerNode
{
    Q_OBJECT
public:
    static DatabaseSyncConnectionServerNodePtr create( const QString &controlId,
                                                       const QTcpSocketPtr socket,
                                                       const QString &lastOperationGuid = "" );

protected:
    void virtual processNewMessage( const MsgPtr &msg );
    virtual void init();

signals:
    void fetchDatabaseOperations( const QString lastDatabaseOperationGuid );

    void tracksAdded( const TPCoreApi::TrackPtrList tracks );
    void tracksRemoved( const QList<qulonglong> trackIds );
    void playlistsCreated( const TPCoreApi::PlaylistPtrList playlists );
    void playlistsDeleted( const QList<QString> playlistIds );
    void playlistChanged( const QString playlistId, const TPCoreApi::PlaylistRevisionPtr revision );
    void playlistRenamed( const QString playlistId, const QString &newTitle );
    void playbackStarted( const QString &artistName, const QString &trackName );
    void playbackFinished( const QString &artistName, const QString &trackName, const quint32 secondsPlayed );
    void trackLoved( const QString &artistName, const QString &trackName );
    void trackUnloved( const QString &artistName, const QString &trackName );

private slots:
    void onDatabaseSynced( const QString lastDatabaseOperationGuid );
    void onTriggerDatabaseOperationFetches();

private:
    explicit DatabaseSyncConnectionServerNode( const QString &controlId,
                                               const QTcpSocketPtr socket,
                                               const QString &lastOperationGuid,
                                               QObject *parent = 0 );

    DatabaseSyncConnectionServerNodeImplPtr m_impl;
};

#endif // DATABASESYNCCONNECTIONSERVERNODE_H
