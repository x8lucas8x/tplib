/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef STREAMCONNECTIONSERVERNODE_H
#define STREAMCONNECTIONSERVERNODE_H

#include "MsgTypedefs.h"
#include "ServerNodeTypedefs.h"
#include "CoreApiTypedefs.h"
#include "ConnectionServerNode.h"

#include <QObject>

class StreamConnectionServerNode : public ConnectionServerNode
{
    Q_OBJECT
public:
    static StreamConnectionServerNodePtr create( const QString &controlId,
                                                 const TPCoreApi::TrackPtr track,
                                                 QTcpSocketPtr socket );

    const QIODevicePtr &resolveTrackBuffer();

protected:
    virtual void processNewMessage( const MsgPtr &msg );
    virtual void init();

private:
    explicit StreamConnectionServerNode( const QString &controlId,
                                         const TPCoreApi::TrackPtr track,
                                         QTcpSocketPtr socket,
                                         QObject *parent = 0 );

    StreamConnectionServerNodeImplPtr m_impl;
};

#endif // STREAMCONNECTIONSERVERNODE_H
