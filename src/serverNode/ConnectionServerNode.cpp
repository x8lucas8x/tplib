/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "ConnectionServerNode.h"

#include <QTimer>

//------------------------------------------------------------------------------
class ConnectionServerNodeImpl
{
public:
    ConnectionServerNodeImpl( const QString &_id )
        : id( _id )
        , setupTimeout( QTimerPtr( new QTimer ) )
    {
        // If this setup exchange is not completed within 3 minutes, then the
        // connection is closed.
        setupTimeout->setInterval( 180000 );
        setupTimeout->start();
    }

    const QString id;
    QTimerPtr setupTimeout;
};
//------------------------------------------------------------------------------

ConnectionServerNode::ConnectionServerNode( const QString &id,
                                            QTcpSocketPtr socket,
                                            QObject *parent )
    : ServerNode( socket, parent )
    , m_impl( ConnectionServerNodeImplPtr( new ConnectionServerNodeImpl( id ) ) )
{
    connect( m_impl->setupTimeout.data(), SIGNAL(timeout()),
             this, SIGNAL(shutdown()) );
}

const QString&
ConnectionServerNode::id() const
{
    return m_impl->id;
}

void ConnectionServerNode::onSetupAndVersionCheckSucceed()
{
    m_impl->setupTimeout.clear();

    init();
}

void ConnectionServerNode::onSetupAndVersionCheckFailed()
{
    disconnect( m_impl->setupTimeout.data(), SIGNAL(timeout()),
             this, SIGNAL(shutdown()) );
    m_impl->setupTimeout.clear();
    emit shutdown();
}
