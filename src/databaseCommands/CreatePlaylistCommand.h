/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef CREATEPLAYLISTCOMMAND_H
#define CREATEPLAYLISTCOMMAND_H

#include "CoreApiTypedefs.h"
#include "DatabaseCommandsTypedefs.h"
#include "DatabaseCommand.h"

//------------------------------------------------------------------------------
class CreatePlaylistCommand : public DatabaseCommand
{
public:
    static CreatePlaylistCommandPtr create();

    virtual const DatabaseCommandType type() const;

    const DatabasePlaylistPtr &playlist() const;
    void setPlaylist( const DatabasePlaylistPtr &playlist );

    virtual bool jsonToDatabaseCommand( const QVariantMap &json );
    virtual const QVariantMap databaseCommandToJson() const;

private:
    explicit CreatePlaylistCommand();
    virtual const QString commandName() const;

    CreatePlaylistCommandImplPtr m_impl;
};
//------------------------------------------------------------------------------
class DatabasePlaylist
{
public:
    static DatabasePlaylistPtr create();

    const QString &playlistInformation() const;
    const QString &playlistCreator() const;
    const QString &playlistTitle() const;
    qulonglong playlistCreatedOn() const;
    const QString &currentRevisionGuid() const;
    bool isShared() const;
    const QString &playlistGuid() const;

    void setPlaylistInformation( const QString &playlistInformation );
    void setPlaylistCreator( const QString &playlistCreator );
    void setPlaylistTitle( const QString &playlistTitle );
    void setPlaylistCreatedOn( const qulonglong &playlistCreatedOn );
    void setCurrentRevisionGuid( const QString &currentRevisionGuid );
    void setIsShared( const bool &isShared );
    void setPlaylistGuid( const QString &guid );

    QVariantMap toJson();
    void fromJson( const QVariantMap &json );
    TPCoreApi::PlaylistPtr toPlaylist();
    void fromPlaylist( const TPCoreApi::PlaylistPtr playlist );

private:
    explicit DatabasePlaylist();

    PlaylistImplPtr m_impl;
};
//------------------------------------------------------------------------------

#endif // CREATEPLAYLISTCOMMAND_H
