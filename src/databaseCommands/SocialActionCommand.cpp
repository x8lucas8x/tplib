/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "SocialActionCommand.h"

#include "SocialActionCommandStrings.h"

//------------------------------------------------------------------------------
class SocialActionCommandImpl
{
public:
    SocialActionCommandImpl()
    {
    }

    QString artistName;
    QString trackName;
    quint64 timestamp;
    SocialActionCommand::ActionType actionType;
};
//------------------------------------------------------------------------------


SocialActionCommand::SocialActionCommand()
    : DatabaseCommand()
    , m_impl( SocialActionCommandImplPtr( new SocialActionCommandImpl ) )
{
}

const SocialActionCommand::ActionType
SocialActionCommand::convertActionTupleToActionType( const QString &actionType, const QString &actionValue ) const
{
    ActionType resultActionType;

    if( actionType == "Love" )
    {
        if( actionValue == "true" )
        {
            resultActionType = SocialActionCommand::Love;
        }
        else if( actionValue == "false" )
        {
            resultActionType = SocialActionCommand::Unlove;
        }
        else
        {
            resultActionType = SocialActionCommand::UnknownActionType;
        }
    }
    else
    {
        resultActionType = SocialActionCommand::UnknownActionType;
    }

    return resultActionType;
}

const QPair<QString, QString>
SocialActionCommand::convertActionTypeToActionTuple( const SocialActionCommand::ActionType &actionType ) const
{
    QPair<QString, QString> resultActionTuple;

    switch( actionType )
    {
    case SocialActionCommand::Love:
    {
        resultActionTuple.first = "Love";
        resultActionTuple.second = "true";
        break;
    }
    case SocialActionCommand::Unlove:
    {
        resultActionTuple.first = "Love";
        resultActionTuple.second = "false";
        break;
    }
    default:
    {
        resultActionTuple.first = "unknown";
        resultActionTuple.second = "unknown";
        break;
    }
    }

    return resultActionTuple;
}

SocialActionCommandPtr
SocialActionCommand::create()
{
    return SocialActionCommandPtr( new SocialActionCommand );
}

const DatabaseCommand::DatabaseCommandType
SocialActionCommand::type() const
{
    return DatabaseCommand::SocialActionCommand;
}

const QString
SocialActionCommand::commandName() const
{
    return ProtocolStr::SocialActionCommandObj::valCommandName();
}

const QString&
SocialActionCommand::artistName() const
{
    return m_impl->artistName;
}

void
SocialActionCommand::setArtistName( const QString &artistName )
{
    m_impl->artistName = artistName;
}

const QString&
SocialActionCommand::trackName() const
{
    return m_impl->trackName;
}

void
SocialActionCommand::setTrackName( const QString &trackName )
{
    m_impl->trackName = trackName;
}

quint64
SocialActionCommand::timestamp() const
{
    return m_impl->timestamp;
}

void
SocialActionCommand::setTimestamp( const quint64 &timestamp )
{
    m_impl->timestamp = timestamp;
}

const SocialActionCommand::ActionType&
SocialActionCommand::actionType() const
{
    return m_impl->actionType;
}

void
SocialActionCommand::setActionType( const SocialActionCommand::ActionType &actionType )
{
    m_impl->actionType = actionType;
}

bool
SocialActionCommand::jsonToDatabaseCommand( const QVariantMap &json )
{
    if( !DatabaseCommand::jsonToDatabaseCommand( json ) )
        return false;

    SocialActionCommand::ActionType actionType;
    actionType = convertActionTupleToActionType(
                json[ProtocolStr::SocialActionCommandObj::actionType()].toString(),
                json[ProtocolStr::SocialActionCommandObj::actionComment()].toString());

    if( actionType == SocialActionCommand::UnknownActionType )
        return false;

    setArtistName( json[ProtocolStr::SocialActionCommandObj::artistName()].toString() );
    setTrackName( json[ProtocolStr::SocialActionCommandObj::trackName()].toString() );
    setActionType( actionType );
    setTimestamp( json[ProtocolStr::SocialActionCommandObj::timestamp()].toLongLong() );

    return true;
}

const QVariantMap
SocialActionCommand::databaseCommandToJson() const
{
    QVariantMap json = DatabaseCommand::databaseCommandToJson();

    if( actionType() == SocialActionCommand::UnknownActionType )
        return json;

    QPair<QString, QString> actionTuple = convertActionTypeToActionTuple( actionType() );
    json[ProtocolStr::SocialActionCommandObj::artistName()] = artistName();
    json[ProtocolStr::SocialActionCommandObj::trackName()] = trackName();
    json[ProtocolStr::SocialActionCommandObj::actionType()] = actionTuple.first;
    json[ProtocolStr::SocialActionCommandObj::actionComment()] = actionTuple.second;
    json[ProtocolStr::SocialActionCommandObj::timestamp()] = timestamp();

    return json;
}
