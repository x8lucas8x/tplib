/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "CreatePlaylistCommand.h"

#include "CreatePlaylistCommandStrings.h"
#include "Playlist.h"

//------------------------------------------------------------------------------
class CreatePlaylistCommandImpl
{
public:
    CreatePlaylistCommandImpl()
    {
    }

    DatabasePlaylistPtr playlist;
};
//------------------------------------------------------------------------------

CreatePlaylistCommand::CreatePlaylistCommand()
    : DatabaseCommand()
    , m_impl( CreatePlaylistCommandImplPtr( new CreatePlaylistCommandImpl ) )
{
}

CreatePlaylistCommandPtr
CreatePlaylistCommand::create()
{
    return CreatePlaylistCommandPtr( new CreatePlaylistCommand );
}

const DatabaseCommand::DatabaseCommandType
CreatePlaylistCommand::type() const
{
    return DatabaseCommand::CreatePlaylistCommand;
}

const QString
CreatePlaylistCommand::commandName() const
{
    return ProtocolStr::CreatePlaylistCommandObj::valCommandName();
}

const DatabasePlaylistPtr&
CreatePlaylistCommand::playlist() const
{
    return m_impl->playlist;
}

void
CreatePlaylistCommand::setPlaylist( const DatabasePlaylistPtr &playlist )
{
    m_impl->playlist = playlist;
}

bool
CreatePlaylistCommand::jsonToDatabaseCommand( const QVariantMap &json )
{
    if( !DatabaseCommand::jsonToDatabaseCommand( json ) )
        return false;

    DatabasePlaylistPtr playlist = DatabasePlaylist::create();
    playlist->fromJson( json[ProtocolStr::CreatePlaylistCommandObj::playlistObj()].toMap() );
    setPlaylist( playlist );

    return true;
}

const QVariantMap
CreatePlaylistCommand::databaseCommandToJson() const
{
    QVariantMap json = DatabaseCommand::databaseCommandToJson();

    json.insert( ProtocolStr::CreatePlaylistCommandObj::playlistObj(), playlist()->toJson() );

    return json;
}

//------------------------------------------------------------------------------
class PlaylistImpl
{
public:
    PlaylistImpl()
    {
    }

    QString playlistInformation;
    QString playlistCreator;
    QString playlistTitle;
    qulonglong playlistCreatedOn;
    QString currentRevision;
    bool isShared;
    QString playlistGuid;
};
//------------------------------------------------------------------------------

DatabasePlaylist::DatabasePlaylist()
    : m_impl( PlaylistImplPtr( new PlaylistImpl ) )
{
}

DatabasePlaylistPtr
DatabasePlaylist::create()
{
    return DatabasePlaylistPtr( new DatabasePlaylist );
}

const QString&
DatabasePlaylist::playlistInformation() const
{
    return m_impl->playlistInformation;
}

const QString&
DatabasePlaylist::playlistCreator() const
{
    return m_impl->playlistCreator;
}

const QString&
DatabasePlaylist::playlistTitle() const
{
    return m_impl->playlistTitle;
}

qulonglong
DatabasePlaylist::playlistCreatedOn() const
{
    return m_impl->playlistCreatedOn;
}

const QString&
DatabasePlaylist::currentRevisionGuid() const
{
    return m_impl->currentRevision;
}

bool
DatabasePlaylist::isShared() const
{
    return m_impl->isShared;
}

const QString&
DatabasePlaylist::playlistGuid() const
{
    return m_impl->playlistGuid;
}

void
DatabasePlaylist::setPlaylistInformation( const QString &playlistInfo )
{
    m_impl->playlistInformation = playlistInfo;
}

void
DatabasePlaylist::setPlaylistCreator( const QString &playlistCreator )
{
    m_impl->playlistCreator = playlistCreator;
}

void
DatabasePlaylist::setPlaylistTitle( const QString &playlistTitle )
{
    m_impl->playlistTitle = playlistTitle;
}

void
DatabasePlaylist::setPlaylistCreatedOn( const qulonglong &playlistCreatedOn )
{
    m_impl->playlistCreatedOn = playlistCreatedOn;
}

void
DatabasePlaylist::setCurrentRevisionGuid( const QString &currentRevision )
{
    m_impl->currentRevision = currentRevision;
}

void
DatabasePlaylist::setIsShared( const bool &isShared )
{
    m_impl->isShared = isShared;
}

void
DatabasePlaylist::setPlaylistGuid( const QString &guid )
{
    m_impl->playlistGuid = guid;
}

QVariantMap
DatabasePlaylist::toJson()
{
    QVariantMap json;

    json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::info()] = playlistInformation();
    json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::creator()] = playlistCreator();
    json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::title()] = playlistTitle();
    json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::createdOn()] = playlistCreatedOn();
    json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::currentRevision()] = currentRevisionGuid();
    json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::isShared()] = isShared();
    json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::playlistGuid()] = playlistGuid();

    return json;
}

void
DatabasePlaylist::fromJson( const QVariantMap &json )
{   
    setPlaylistInformation( json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::info()].toString() );
    setPlaylistCreator( json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::creator()].toString() );
    setPlaylistTitle( json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::title()].toString() );
    setPlaylistCreatedOn( json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::createdOn()].toULongLong() );
    setCurrentRevisionGuid( json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::currentRevision()].toString() );
    setIsShared( json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::isShared()].toBool() );
    setPlaylistGuid( json[ProtocolStr::CreatePlaylistCommandObj::PlaylistObj::playlistGuid()].toString() );
}

TPCoreApi::PlaylistPtr
DatabasePlaylist::toPlaylist()
{
    TPCoreApi::PlaylistPtr playlist = TPCoreApi::Playlist::create();

    playlist->setCreatedOn( playlistCreatedOn() );
    playlist->setCreatorName( playlistCreator() );
    playlist->setCurrentRevisionGuid( currentRevisionGuid() );
    playlist->setGuid( playlistGuid() );
    playlist->setInformation( playlistInformation() );
    playlist->setIsShared( isShared() );
    playlist->setTitle( playlistTitle() );

    return playlist;
}

void
DatabasePlaylist::fromPlaylist( const TPCoreApi::PlaylistPtr playlist )
{
    setPlaylistInformation( playlist->information() );
    setPlaylistCreator( playlist->creatorName() );
    setPlaylistTitle( playlist->title() );
    setPlaylistCreatedOn( playlist->createdOn() );
    setCurrentRevisionGuid( playlist->currentRevisionGuid() );
    setIsShared( playlist->isShared() );
    setPlaylistGuid( playlist->guid() );
}
