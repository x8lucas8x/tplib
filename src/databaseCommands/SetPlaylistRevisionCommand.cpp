/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "SetPlaylistRevisionCommand.h"

#include "SetPlaylistRevisionCommandStrings.h"
#include "GlobalUtilityFunctions.h"

//------------------------------------------------------------------------------
class SetPlaylistRevisionCommandImpl
{
public:
    SetPlaylistRevisionCommandImpl()
    {
    }

    QString oldRevision;
    QString newRevision;
    QList<QString> orderedEntryGuids;
    PlaylistRevisionEntryPtrList addedEntries;
};
//------------------------------------------------------------------------------

SetPlaylistRevisionCommand::SetPlaylistRevisionCommand()
    : PlaylistDatabaseCommand()
    , m_impl( SetPlaylistRevisionCommandImplPtr( new SetPlaylistRevisionCommandImpl ) )
{
}


SetPlaylistRevisionCommandPtr
SetPlaylistRevisionCommand::create()
{
    return SetPlaylistRevisionCommandPtr( new SetPlaylistRevisionCommand );
}

const DatabaseCommand::DatabaseCommandType
SetPlaylistRevisionCommand::type() const
{
    return DatabaseCommand::SetPlaylistRevisionCommand;
}

const QString
SetPlaylistRevisionCommand::commandName() const
{
    return ProtocolStr::SetPlaylistRevisionCommandObj::valCommandName();
}

const QString&
SetPlaylistRevisionCommand::oldRevision() const
{
    return m_impl->oldRevision;
}

void
SetPlaylistRevisionCommand::setOldRevision( const QString &guid )
{
    m_impl->oldRevision = guid;
}

const QString&
SetPlaylistRevisionCommand::newRevision() const
{
    return m_impl->newRevision;
}

void
SetPlaylistRevisionCommand::setNewRevision( const QString &guid )
{
    m_impl->newRevision = guid;
}

const PlaylistRevisionEntryPtrList&
SetPlaylistRevisionCommand::addedEntries() const
{
    return m_impl->addedEntries;
}

void
SetPlaylistRevisionCommand::setAddedEntries( const PlaylistRevisionEntryPtrList &entries )
{
    m_impl->addedEntries = entries;
}

const QList<QString>&
SetPlaylistRevisionCommand::orderedEntryGuids() const
{
    return m_impl->orderedEntryGuids;
}

void
SetPlaylistRevisionCommand::setOrderedEntryGuids( const QList<QString> &entryGuids )
{
    m_impl->orderedEntryGuids = entryGuids;
}

bool
SetPlaylistRevisionCommand::jsonToDatabaseCommand( const QVariantMap &json )
{
    if( !PlaylistDatabaseCommand::jsonToDatabaseCommand( json ) )
        return false;

    setOldRevision( json[ProtocolStr::SetPlaylistRevisionCommandObj::oldRevision()].toString() );
    setNewRevision( json[ProtocolStr::SetPlaylistRevisionCommandObj::newRevision()].toString() );

    PlaylistRevisionEntryPtrList addedEntries;

    foreach( const QVariant &variant,
             json[ProtocolStr::SetPlaylistRevisionCommandObj::addedEntriesArray()].toList() )
    {
       PlaylistRevisionEntryPtr entry = PlaylistRevisionEntry::create();
       entry->fromJson( variant.toMap() );
       addedEntries.append( entry );
    }

    setAddedEntries( addedEntries );

    QList<QString> orderedGuids;

    foreach( const QVariant &variant,
             json[ProtocolStr::SetPlaylistRevisionCommandObj::orderedGuidsArray()].toList() )
    {
       orderedGuids.append( variant.toString() );
    }

    setOrderedEntryGuids( orderedGuids );

    return true;
}

const QVariantMap
SetPlaylistRevisionCommand::databaseCommandToJson() const
{
    QVariantMap json = PlaylistDatabaseCommand::databaseCommandToJson();

    json[ProtocolStr::SetPlaylistRevisionCommandObj::oldRevision()] = oldRevision();
    json[ProtocolStr::SetPlaylistRevisionCommandObj::newRevision()] = newRevision();

    QVariantList addedEntriesArray;

    foreach( const PlaylistRevisionEntryPtr &entry, addedEntries() )
    {
        addedEntriesArray.append( entry->toJson() );
    }

    json.insert( ProtocolStr::SetPlaylistRevisionCommandObj::addedEntriesArray(),
                 addedEntriesArray );

    QVariantList orderedGuidsArray;

    foreach( const QString &guid, orderedEntryGuids() )
    {
        orderedGuidsArray.append( guid );
    }

    json.insert( ProtocolStr::SetPlaylistRevisionCommandObj::orderedGuidsArray(),
                 orderedGuidsArray );

    return json;
}

TPCoreApi::PlaylistRevisionPtr
SetPlaylistRevisionCommand::toPlaylistRevision()
{
    TPCoreApi::PlaylistRevisionPtr playlistRevision = TPCoreApi::PlaylistRevision::create();
    QList<TPCoreApi::PlaylistRevisionEntryPtr> revisionEntryList;

    foreach( const PlaylistRevisionEntryPtr tempRevisionEntry, addedEntries() )
    {
        TPCoreApi::PlaylistRevisionEntryPtr revisionEntry = TPCoreApi::PlaylistRevisionEntry::create();
        revisionEntry->setAlbumName( tempRevisionEntry->query()->albumName() );
        revisionEntry->setArtistName( tempRevisionEntry->query()->artistName() );
        revisionEntry->setTrackName( tempRevisionEntry->query()->trackName() );
        revisionEntry->setEntryGuid( tempRevisionEntry->entryGuid() );
        revisionEntry->setLastModified( tempRevisionEntry->lastModified() );
        revisionEntry->setTrackDuration( tempRevisionEntry->trackDuration() );

        revisionEntryList.append( revisionEntry );
    }

    playlistRevision->setAddedEntries( revisionEntryList );
    playlistRevision->setOrderedEntryGuids( orderedEntryGuids() );
    playlistRevision->setRevisionGuid( newRevision() );

    return playlistRevision;
}

void
SetPlaylistRevisionCommand::fromPlaylistRevision( const TPCoreApi::PlaylistRevisionPtr playlistRevision )
{
    QList<PlaylistRevisionEntryPtr> revisionEntryList;

    foreach( const TPCoreApi::PlaylistRevisionEntryPtr tempRevisionEntry, playlistRevision->addedEntries() )
    {
        PlaylistRevisionEntryPtr revisionEntry = PlaylistRevisionEntry::create();
        QueryPtr query = Query::create();

        query->setQueryId( GlobalUtils::generateNewUuidString() );
        query->setAlbumName( tempRevisionEntry->albumName() );
        query->setArtistName( tempRevisionEntry->artistName() );
        query->setTrackName( tempRevisionEntry->trackName() );
        query->setTrackDuration( tempRevisionEntry->trackDuration() );

        revisionEntry->setAnnotation( "" );
        revisionEntry->setQuery( query );
        revisionEntry->setEntryGuid( tempRevisionEntry->entryGuid() );
        revisionEntry->setLastModified( tempRevisionEntry->lastModified() );
        revisionEntry->setTrackDuration( tempRevisionEntry->trackDuration() );

        revisionEntryList.append( revisionEntry );
    }

    setAddedEntries( revisionEntryList );
    setOrderedEntryGuids( playlistRevision->orderedEntryGuids() );
    setOldRevision( "" );
    setNewRevision( playlistRevision->revisionGuid() );
}

//------------------------------------------------------------------------------
class PlaylistRevisionEntryImpl
{
public:
    PlaylistRevisionEntryImpl()
    {
    }

    QString entryGuid;
    qulonglong trackDuration;
    qulonglong lastModified;
    QString annotation;
    QueryPtr query;
};
//------------------------------------------------------------------------------

PlaylistRevisionEntry::PlaylistRevisionEntry()
    : m_impl( PlaylistRevisionEntryImplPtr( new PlaylistRevisionEntryImpl ) )

{
}

PlaylistRevisionEntryPtr
PlaylistRevisionEntry::create()
{
    return PlaylistRevisionEntryPtr( new PlaylistRevisionEntry );
}

const QString&
PlaylistRevisionEntry::entryGuid() const
{
    return m_impl->entryGuid;
}

qulonglong
PlaylistRevisionEntry::trackDuration() const
{
    return m_impl->trackDuration;
}

qulonglong
PlaylistRevisionEntry::lastModified() const
{
    return m_impl->lastModified;
}

const QString&
PlaylistRevisionEntry::annotation() const
{
    return m_impl->annotation;
}

const QueryPtr&
PlaylistRevisionEntry::query() const
{
    return m_impl->query;
}

void
PlaylistRevisionEntry::setEntryGuid( const QString &guid )
{
    m_impl->entryGuid = guid;
}

void
PlaylistRevisionEntry::setTrackDuration( const qulonglong duration )
{
    m_impl->trackDuration = duration;
}

void
PlaylistRevisionEntry::setLastModified( const qulonglong lastModified )
{
    m_impl->lastModified = lastModified;
}

void
PlaylistRevisionEntry::setAnnotation( const QString &annotation )
{
    m_impl->annotation = annotation;
}

void
PlaylistRevisionEntry::setQuery( const QueryPtr &query )
{
    m_impl->query = query;
}

QVariantMap
PlaylistRevisionEntry::toJson()
{
    QVariantMap json;

    json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::entryGuid()] = entryGuid();
    json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::duration()] = trackDuration();
    json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::lastModified()] = lastModified();
    json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::annotation()] = annotation();

    json.insert( ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::queryObj(), query()->toJson() );

    return json;
}

void
PlaylistRevisionEntry::fromJson( const QVariantMap &json )
{
    setEntryGuid( json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::entryGuid()].toString() );
    setTrackDuration( json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::duration()].toULongLong() );
    setLastModified( json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::lastModified()].toULongLong() );
    setAnnotation( json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::annotation()].toString() );

    QueryPtr query = Query::create();
    query->fromJson( json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::queryObj()].toMap() );
    setQuery( query );
}

//------------------------------------------------------------------------------
class QueryImpl
{
public:
    QueryImpl()
    {
    }

    QString queryId;
    QString trackName;
    QString artistName;
    QString albumName;
    qulonglong trackDuration;
};
//------------------------------------------------------------------------------

Query::Query()
    : m_impl( QueryImplPtr( new QueryImpl ) )

{
}

QueryPtr
Query::create()
{
    return QueryPtr( new Query );
}

const QString&
Query::queryId() const
{
    return m_impl->queryId;
}

const QString&
Query::trackName() const
{
    return m_impl->trackName;
}

const QString&
Query::artistName() const
{
    return m_impl->artistName;
}

const QString&
Query::albumName() const
{
    return m_impl->albumName;
}

qulonglong
Query::trackDuration() const
{
    return m_impl->trackDuration;
}

void
Query::setQueryId( const QString &id )
{
    m_impl->queryId = id;
}

void
Query::setTrackName( const QString &name )
{
    m_impl->trackName = name;
}

void
Query::setArtistName( const QString &name )
{
    m_impl->artistName = name;
}

void
Query::setAlbumName( const QString &name )
{
    m_impl->albumName = name;
}

void
Query::setTrackDuration( const qulonglong duration )
{
    m_impl->trackDuration = duration;
}

QVariantMap
Query::toJson()
{
    QVariantMap json;

    json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::QueryObj::queryId()] = queryId();
    json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::QueryObj::trackName()] = trackName();
    json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::QueryObj::artistName()] = artistName();
    json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::QueryObj::albumName()] = albumName();
    json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::QueryObj::duration()] = trackDuration();

    return json;
}

void
Query::fromJson( const QVariantMap &json )
{
    setQueryId( json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::QueryObj::queryId()].toString() );
    setTrackName( json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::QueryObj::trackName()].toString() );
    setArtistName( json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::QueryObj::artistName()].toString() );
    setAlbumName( json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::QueryObj::albumName()].toString() );
    setTrackDuration( json[ProtocolStr::SetPlaylistRevisionCommandObj::AddedEntryObj::QueryObj::duration()].toULongLong() );
}
