/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef SOCIALACTIONCOMMAND_H
#define SOCIALACTIONCOMMAND_H

#include "DatabaseCommandsTypedefs.h"
#include "DatabaseCommand.h"

class SocialActionCommand : public DatabaseCommand
{
public:
    enum ActionType { UnknownActionType = 0, Love, Unlove };

    static SocialActionCommandPtr create();

    virtual const DatabaseCommandType type() const;

    const QString &artistName() const;
    void setArtistName( const QString &artistName );

    const QString &trackName() const;
    void setTrackName( const QString &trackName );

    quint64 timestamp() const;
    void setTimestamp( const quint64 &timestamp );

    const ActionType &actionType() const;
    void setActionType( const ActionType &actionType );

    virtual bool jsonToDatabaseCommand( const QVariantMap &json );
    virtual const QVariantMap databaseCommandToJson() const;

private:
    explicit SocialActionCommand();
    virtual const QString commandName() const;
    const ActionType convertActionTupleToActionType( const QString &actionType,
                                                      const QString &actionValue ) const;
    const QPair<QString, QString> convertActionTypeToActionTuple( const ActionType &actionType ) const;

    SocialActionCommandImplPtr m_impl;
};

#endif // SOCIALACTIONCOMMAND_H
