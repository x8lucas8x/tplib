/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef ADDFILESCOMMAND_H
#define ADDFILESCOMMAND_H

#include "CoreApiTypedefs.h"
#include "DatabaseCommandsTypedefs.h"
#include "DatabaseCommand.h"

//------------------------------------------------------------------------------
class AddFilesCommand : public DatabaseCommand
{
public:
    static AddFilesCommandPtr create();

    virtual const DatabaseCommandType type() const;

    const QList<TrackPtr> &tracks() const;
    void setTracks( const QList<TrackPtr> &tracks );
    void appendTracks( const QList<TrackPtr> &tracks );

    virtual bool jsonToDatabaseCommand( const QVariantMap &json );
    virtual const QVariantMap databaseCommandToJson() const;

private:
    explicit AddFilesCommand();
    virtual const QString commandName() const;

    AddFilesCommandImplPtr m_impl;
};
//------------------------------------------------------------------------------
class Track
{
public:
    static TrackPtr create();

    qulonglong trackId() const;
    const QString &trackUrl() const;
    const QString &artistName() const;
    const QString &albumName() const;
    const QString &trackName() const;
    const QString &mimeType() const;
    const QString &trackHash() const;
    uint trackYear() const;
    uint albumPosition() const;
    qulonglong trackModifiedTime() const;
    qulonglong trackDuration() const;
    uint trackBitRate() const;
    qulonglong trackSize() const;

    void setTrackId( const qulonglong trackId );
    void setTrackUrl( const QString &trackUrl );
    void setTrackArtistName( const QString &artistName );
    void setTrackAlbumName( const QString &albumName );
    void setTrackName( const QString &trackName );
    void setTrackMimeType( const QString &mimeType );
    void setTrackHash( const QString &hash );
    void setTrackYear( const uint year );
    void setTrackAlbumPosition( const uint position );
    void setTrackModifiedTime( const qulonglong mTime );
    void setTrackDuration( const qulonglong duration );
    void setTrackBitRate( const uint bitRate );
    void setTrackSize( const qulonglong size );

    QVariantMap toJson();
    void fromJson( const QVariantMap &json );
    TPCoreApi::TrackPtr toTrack();
    void fromTrack( const TPCoreApi::TrackPtr track );

private:
    explicit Track();

    TrackImplPtr m_impl;
};
//------------------------------------------------------------------------------

#endif // ADDFILESCOMMAND_H
