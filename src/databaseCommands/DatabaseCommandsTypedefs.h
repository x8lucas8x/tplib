/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef DATABASECOMMANDSTYPEDEFS_H
#define DATABASECOMMANDSTYPEDEFS_H

#include <QList>
#include <QSharedPointer>

class DatabaseCommand;
typedef QSharedPointer<DatabaseCommand> DatabaseCommandPtr;
class DatabaseCommandImpl;
typedef QSharedPointer<DatabaseCommandImpl> DatabaseCommandImplPtr;
typedef QList<DatabaseCommandPtr> DatabaseCommandPtrList;

class AddFilesCommand;
typedef QSharedPointer<AddFilesCommand> AddFilesCommandPtr;
class AddFilesCommandImpl;
typedef QSharedPointer<AddFilesCommandImpl> AddFilesCommandImplPtr;

class Track;
typedef QSharedPointer<Track> TrackPtr;
class TrackImpl;
typedef QSharedPointer<TrackImpl> TrackImplPtr;
typedef QList<TrackPtr> TrackPtrList;

class DeleteFilesCommand;
typedef QSharedPointer<DeleteFilesCommand> DeleteFilesCommandPtr;
class DeleteFilesCommandImpl;
typedef QSharedPointer<DeleteFilesCommandImpl> DeleteFilesCommandImplPtr;

class DatabasePlaylist;
typedef QSharedPointer<DatabasePlaylist> DatabasePlaylistPtr;
class PlaylistImpl;
typedef QSharedPointer<PlaylistImpl> PlaylistImplPtr;

class CreatePlaylistCommand;
typedef QSharedPointer<CreatePlaylistCommand> CreatePlaylistCommandPtr;
class CreatePlaylistCommandImpl;
typedef QSharedPointer<CreatePlaylistCommandImpl> CreatePlaylistCommandImplPtr;

class PlaylistDatabaseCommand;
typedef QSharedPointer<PlaylistDatabaseCommand> PlaylistDatabaseCommandPtr;
class PlaylistDatabaseCommandImpl;
typedef QSharedPointer<PlaylistDatabaseCommandImpl> PlaylistDatabaseCommandImplPtr;

class Query;
typedef QSharedPointer<Query> QueryPtr;
class QueryImpl;
typedef QSharedPointer<QueryImpl> QueryImplPtr;

class PlaylistRevisionEntry;
typedef QSharedPointer<PlaylistRevisionEntry> PlaylistRevisionEntryPtr;
class PlaylistRevisionEntryImpl;
typedef QSharedPointer<PlaylistRevisionEntryImpl> PlaylistRevisionEntryImplPtr;
typedef QList<PlaylistRevisionEntryPtr> PlaylistRevisionEntryPtrList;

class SetPlaylistRevisionCommand;
typedef QSharedPointer<SetPlaylistRevisionCommand> SetPlaylistRevisionCommandPtr;
class SetPlaylistRevisionCommandImpl;
typedef QSharedPointer<SetPlaylistRevisionCommandImpl> SetPlaylistRevisionCommandImplPtr;

class RenamePlaylistCommand;
typedef QSharedPointer<RenamePlaylistCommand> RenamePlaylistCommandPtr;
class RenamePlaylistCommandImpl;
typedef QSharedPointer<RenamePlaylistCommandImpl> RenamePlaylistCommandImplPtr;

class DeletePlaylistCommand;
typedef QSharedPointer<DeletePlaylistCommand> DeletePlaylistCommandPtr;

class SocialActionCommand;
typedef QSharedPointer<SocialActionCommand> SocialActionCommandPtr;
class SocialActionCommandImpl;
typedef QSharedPointer<SocialActionCommandImpl> SocialActionCommandImplPtr;

class LogPlaybackCommand;
typedef QSharedPointer<LogPlaybackCommand> LogPlaybackCommandPtr;
class LogPlaybackCommandImpl;
typedef QSharedPointer<LogPlaybackCommandImpl> LogPlaybackCommandImplPtr;

class DatabaseCommandFactoryWorker;
class DatabaseCommandFactoryWorkerImpl;
typedef QSharedPointer<DatabaseCommandFactoryWorkerImpl> DatabaseCommandFactoryWorkerImplPtr;

#endif // DATABASECOMMANDSTYPEDEFS_H
