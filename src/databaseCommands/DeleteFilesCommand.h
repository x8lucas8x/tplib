/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef DELETEFILESCOMMAND_H
#define DELETEFILESCOMMAND_H

#include "DatabaseCommandsTypedefs.h"
#include "DatabaseCommand.h"

class DeleteFilesCommand : public DatabaseCommand
{
public:
    static DeleteFilesCommandPtr create();

    virtual const DatabaseCommandType type() const;

    const QList<qulonglong> &fileIds() const;
    void setFileIds( const QList<qulonglong> &fileIds );
    void appendFileIds( const QList<qulonglong> &fileIds );

    virtual bool jsonToDatabaseCommand( const QVariantMap &json );
    virtual const QVariantMap databaseCommandToJson() const;

signals:
    
private:
    explicit DeleteFilesCommand();
    virtual const QString commandName() const;

    DeleteFilesCommandImplPtr m_impl;
};

#endif // DELETEFILESCOMMAND_H
