/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "DeletePlaylistCommand.h"

#include "DeletePlaylistCommandStrings.h"

DeletePlaylistCommand::DeletePlaylistCommand()
    : PlaylistDatabaseCommand()
{
}

DeletePlaylistCommandPtr
DeletePlaylistCommand::create()
{
    return DeletePlaylistCommandPtr( new DeletePlaylistCommand );
}

const DatabaseCommand::DatabaseCommandType
DeletePlaylistCommand::type() const
{
    return DatabaseCommand::DeletePlaylistCommand;
}

const QString
DeletePlaylistCommand::commandName() const
{
    return ProtocolStr::DeletePlaylistCommandObj::valCommandName();
}

bool
DeletePlaylistCommand::jsonToDatabaseCommand( const QVariantMap &json )
{
    if( !PlaylistDatabaseCommand::jsonToDatabaseCommand( json ) )
        return false;

    return true;
}

const QVariantMap
DeletePlaylistCommand::databaseCommandToJson() const
{
    QVariantMap json = PlaylistDatabaseCommand::databaseCommandToJson();

    return json;
}
