/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "AddFilesCommand.h"

#include "AddFilesCommandStrings.h"
#include "Track.h"

//------------------------------------------------------------------------------
class AddFilesCommandImpl
{
public:
    AddFilesCommandImpl()
    {
    }

    QList<TrackPtr> files;
};
//------------------------------------------------------------------------------

AddFilesCommand::AddFilesCommand()
    : DatabaseCommand()
    , m_impl( AddFilesCommandImplPtr( new AddFilesCommandImpl() ) )
{
}

AddFilesCommandPtr
AddFilesCommand::create()
{
    return AddFilesCommandPtr( new AddFilesCommand );
}

const DatabaseCommand::DatabaseCommandType
AddFilesCommand::type() const
{
    return DatabaseCommand::AddFilesCommand;
}

const QString
AddFilesCommand::commandName() const
{
    return ProtocolStr::AddFilesCommandObj::valCommandName();
}

const QList<TrackPtr>&
AddFilesCommand::tracks() const
{
    return m_impl->files;
}

void
AddFilesCommand::setTracks( const QList<TrackPtr> &files )
{
    m_impl->files = files;
}

void
AddFilesCommand::appendTracks( const QList<TrackPtr> &files )
{
    m_impl->files.append( files );
}

bool
AddFilesCommand::jsonToDatabaseCommand( const QVariantMap &json )
{
    if( !DatabaseCommand::jsonToDatabaseCommand( json ) )
        return false;

    QList<TrackPtr> files;

    foreach( const QVariant &variant,
             json[ProtocolStr::AddFilesCommandObj::filesArray()].toList() )
    {
       TrackPtr file = Track::create();
       file->fromJson( variant.toMap() );
       files.append( file );
    }

    setTracks( files );

    return true;
}

const QVariantMap
AddFilesCommand::databaseCommandToJson() const
{
    QVariantMap json = DatabaseCommand::databaseCommandToJson();
    QVariantList filesArray;  

    foreach( const TrackPtr& track, tracks() )
    {
        filesArray.append( track->toJson() );
    }

    json.insert( ProtocolStr::AddFilesCommandObj::filesArray(), filesArray );

    return json;
}

//------------------------------------------------------------------------------
class TrackImpl
{
public:
    TrackImpl()
    {
    }

    qulonglong trackId;
    QString trackUrl;
    QString trackArtistName;
    QString trackAlbumName;
    QString trackName;
    QString trackMimeType;
    QString trackHash;
    uint trackYear;
    uint trackAlbumPosition;
    qulonglong trackMTime;
    qulonglong trackDuration;
    uint trackBitRate;
    qulonglong trackSize;
};
//------------------------------------------------------------------------------

Track::Track()
    : m_impl( TrackImplPtr( new TrackImpl ) )
{
}

TrackPtr
Track::create()
{
    return TrackPtr( new Track );
}

qulonglong
Track::trackId() const
{
    return m_impl->trackId;
}

const QString&
Track::trackUrl() const
{
    return m_impl->trackUrl;
}

const QString&
Track::artistName() const
{
    return m_impl->trackArtistName;
}

const QString&
Track::albumName() const
{
    return m_impl->trackAlbumName;
}

const QString&
Track::trackName() const
{
    return m_impl->trackName;
}

const QString&
Track::mimeType() const
{
    return m_impl->trackMimeType;
}

const QString&
Track::trackHash() const
{
    return m_impl->trackHash;
}

uint
Track::trackYear() const
{
    return m_impl->trackYear;
}

uint
Track::albumPosition() const
{
    return m_impl->trackAlbumPosition;
}

qulonglong
Track::trackModifiedTime() const
{
    return m_impl->trackMTime;
}

qulonglong
Track::trackDuration() const
{
    return m_impl->trackDuration;
}

uint
Track::trackBitRate() const
{
    return m_impl->trackBitRate;
}

qulonglong
Track::trackSize() const
{
    return m_impl->trackSize;
}

void
Track::setTrackId( const qulonglong trackId )
{
    m_impl->trackId = trackId;
}

void
Track::setTrackUrl( const QString &trackUrl )
{
    m_impl->trackUrl = trackUrl;
}

void
Track::setTrackArtistName( const QString &artistName )
{
    m_impl->trackArtistName = artistName;
}

void
Track::setTrackAlbumName( const QString &albumName )
{
    m_impl->trackAlbumName = albumName;
}

void
Track::setTrackName( const QString &trackName )
{
    m_impl->trackName = trackName;
}

void
Track::setTrackMimeType( const QString &mimeType )
{
    m_impl->trackMimeType = mimeType;
}

void
Track::setTrackHash( const QString &hash )
{
    m_impl->trackHash = hash;
}

void
Track::setTrackYear( const uint year )
{
    m_impl->trackYear = year;
}

void
Track::setTrackAlbumPosition( const uint position )
{
    m_impl->trackAlbumPosition = position;
}

void
Track::setTrackModifiedTime( const qulonglong mTime )
{
    m_impl->trackMTime = mTime;
}

void
Track::setTrackDuration( const qulonglong duration )
{
    m_impl->trackDuration = duration;
}

void
Track::setTrackBitRate( const uint bitRate )
{
    m_impl->trackBitRate = bitRate;
}

void
Track::setTrackSize( const qulonglong size )
{
    m_impl->trackSize = size;
}

QVariantMap
Track::toJson()
{
    QVariantMap json;

    json[ProtocolStr::AddFilesCommandObj::FilesObj::trackId()] = trackId();
    json[ProtocolStr::AddFilesCommandObj::FilesObj::trackUrl()] = trackUrl();
    json[ProtocolStr::AddFilesCommandObj::FilesObj::artistName()] = artistName();
    json[ProtocolStr::AddFilesCommandObj::FilesObj::albumName()] = albumName();
    json[ProtocolStr::AddFilesCommandObj::FilesObj::trackName()] = trackName();
    json[ProtocolStr::AddFilesCommandObj::FilesObj::trackMimeType()] = mimeType();
    json[ProtocolStr::AddFilesCommandObj::FilesObj::trackHash()] = trackHash();
    json[ProtocolStr::AddFilesCommandObj::FilesObj::trackYear()] = trackYear();
    json[ProtocolStr::AddFilesCommandObj::FilesObj::trackAlbumPosition()] = albumPosition();
    json[ProtocolStr::AddFilesCommandObj::FilesObj::trackModifiedTime()] = trackModifiedTime();
    json[ProtocolStr::AddFilesCommandObj::FilesObj::trackDuration()] = trackDuration();
    json[ProtocolStr::AddFilesCommandObj::FilesObj::trackBitRate()] = trackBitRate();
    json[ProtocolStr::AddFilesCommandObj::FilesObj::trackSize()] = trackSize();

    return json;
}

void
Track::fromJson( const QVariantMap &json )
{
    setTrackId( json[ProtocolStr::AddFilesCommandObj::FilesObj::trackId()].toLongLong() );
    setTrackUrl( json[ProtocolStr::AddFilesCommandObj::FilesObj::trackUrl()].toString() );
    setTrackArtistName( json[ProtocolStr::AddFilesCommandObj::FilesObj::artistName()].toString() );
    setTrackAlbumName( json[ProtocolStr::AddFilesCommandObj::FilesObj::albumName()].toString() );
    setTrackName( json[ProtocolStr::AddFilesCommandObj::FilesObj::trackName()].toString() );
    setTrackMimeType( json[ProtocolStr::AddFilesCommandObj::FilesObj::trackMimeType()].toString() );
    setTrackHash( json[ProtocolStr::AddFilesCommandObj::FilesObj::trackHash()].toString() );
    setTrackYear( json[ProtocolStr::AddFilesCommandObj::FilesObj::trackYear()].toLongLong() );
    setTrackAlbumPosition( json[ProtocolStr::AddFilesCommandObj::FilesObj::trackAlbumPosition()].toLongLong() );
    setTrackModifiedTime( json[ProtocolStr::AddFilesCommandObj::FilesObj::trackModifiedTime()].toLongLong() );
    setTrackDuration( json[ProtocolStr::AddFilesCommandObj::FilesObj::trackDuration()].toLongLong() );
    setTrackBitRate( json[ProtocolStr::AddFilesCommandObj::FilesObj::trackBitRate()].toLongLong() );
    setTrackSize( json[ProtocolStr::AddFilesCommandObj::FilesObj::trackSize()].toLongLong() );
}

TPCoreApi::TrackPtr
Track::toTrack()
{
    TPCoreApi::TrackPtr track = TPCoreApi::Track::create();

    track->setId( trackId() );
    track->setUrl( trackUrl() );
    track->setArtistName( artistName() );
    track->setAlbumName( albumName() );
    track->setName( trackName() );
    track->setMimeType( mimeType() );
    track->setHash( trackHash() );
    track->setYear( trackYear() );
    track->setAlbumPosition( albumPosition() );
    track->setModifiedTime( trackModifiedTime() );
    track->setDuration( trackDuration() );
    track->setBitRate( trackBitRate() );
    track->setSize( trackSize() );

    return track;
}

void
Track::fromTrack( const TPCoreApi::TrackPtr track )
{
    setTrackId( track->id() );
    setTrackUrl( track->url() );
    setTrackArtistName( track->artistName() );
    setTrackAlbumName( track->albumName() );
    setTrackName( track->name() );
    setTrackMimeType( track->mimeType() );
    setTrackHash( track->hash() );
    setTrackYear( track->year() );
    setTrackAlbumPosition( track->albumPosition() );
    setTrackModifiedTime( track->modifiedTime() );
    setTrackDuration( track->duration() );
    setTrackBitRate( track->bitRate() );
    setTrackSize( track->size() );
}
