/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "DeleteFilesCommand.h"

#include "DeleteFilesCommandStrings.h"

//------------------------------------------------------------------------------
class DeleteFilesCommandImpl
{
public:
    DeleteFilesCommandImpl()
    {
    }

    QList<qulonglong> fileIds;
};
//------------------------------------------------------------------------------

DeleteFilesCommand::DeleteFilesCommand()
    : DatabaseCommand()
    , m_impl( new DeleteFilesCommandImpl )
{
}

DeleteFilesCommandPtr
DeleteFilesCommand::create()
{
    return DeleteFilesCommandPtr( new DeleteFilesCommand );
}

const DatabaseCommand::DatabaseCommandType
DeleteFilesCommand::type() const
{
    return DatabaseCommand::DeleteFilesCommand;
}

const QString
DeleteFilesCommand::commandName() const
{
    return ProtocolStr::DeleteFilesCommandObj::valCommandName();
}

const QList<qulonglong>&
DeleteFilesCommand::fileIds() const
{
    return m_impl->fileIds;
}

void
DeleteFilesCommand::setFileIds( const QList<qulonglong> &fileIds )
{
    m_impl->fileIds = fileIds;
}

void
DeleteFilesCommand::appendFileIds( const QList<qulonglong> &fileIds )
{
    m_impl->fileIds.append( fileIds );
}

bool
DeleteFilesCommand::jsonToDatabaseCommand( const QVariantMap &json )
{
    if( !DatabaseCommand::jsonToDatabaseCommand( json ) )
        return false;

    QList<qulonglong> fileIds;

    foreach( const QVariant &variant, json[ProtocolStr::DeleteFilesCommandObj::fileIdsArray()].toList() )
    {
       fileIds.append( variant.toLongLong() );
    }

    setFileIds( fileIds );

    return true;
}

const QVariantMap
DeleteFilesCommand::databaseCommandToJson() const
{
    QVariantMap json = DatabaseCommand::databaseCommandToJson();
    QVariantList fileIdsArray;

    foreach( const qulonglong &fileId, fileIds() )
    {
        fileIdsArray.append( fileId );
    }

    json.insert( ProtocolStr::DeleteFilesCommandObj::fileIdsArray(), fileIdsArray );

    return json;
}
