/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef SETPLAYLISTREVISIONCOMMAND_H
#define SETPLAYLISTREVISIONCOMMAND_H

#include "Playlist.h"
#include "CoreApiTypedefs.h"
#include "DatabaseCommandsTypedefs.h"
#include "PlaylistDatabaseCommand.h"

//------------------------------------------------------------------------------
class SetPlaylistRevisionCommand : public PlaylistDatabaseCommand
{
public:
    static SetPlaylistRevisionCommandPtr create();

    virtual const DatabaseCommandType type() const;

    const QString &oldRevision() const;
    void setOldRevision( const QString &guid );

    const QString &newRevision() const;
    void setNewRevision( const QString &guid );

    const PlaylistRevisionEntryPtrList &addedEntries() const;
    void setAddedEntries( const PlaylistRevisionEntryPtrList &entries );

    const QList<QString> &orderedEntryGuids() const;
    void setOrderedEntryGuids( const QList<QString> &entryGuids );

    virtual bool jsonToDatabaseCommand( const QVariantMap &json );
    virtual const QVariantMap databaseCommandToJson() const;
    TPCoreApi::PlaylistRevisionPtr toPlaylistRevision();
    void fromPlaylistRevision( const TPCoreApi::PlaylistRevisionPtr playlistRevision );

private:
    explicit SetPlaylistRevisionCommand();
    virtual const QString commandName() const;

    SetPlaylistRevisionCommandImplPtr m_impl;
};
//------------------------------------------------------------------------------
class PlaylistRevisionEntry
{
public:
    static PlaylistRevisionEntryPtr create();

    const QString &entryGuid() const;
    qulonglong trackDuration() const;
    qulonglong lastModified() const;
    const QString &annotation() const;
    const QueryPtr &query() const;

    void setEntryGuid( const QString &guid );
    void setTrackDuration( const qulonglong duration );
    void setLastModified( const qulonglong lastModified );
    void setAnnotation( const QString &annotation );
    void setQuery( const QueryPtr &query );

    QVariantMap toJson();
    void fromJson( const QVariantMap &json );

private:
    explicit PlaylistRevisionEntry();

    PlaylistRevisionEntryImplPtr m_impl;
};
//------------------------------------------------------------------------------
class Query
{
public:
    static QueryPtr create();


    const QString &queryId() const;
    const QString &trackName() const;
    const QString &artistName() const;
    const QString &albumName() const;
    qulonglong trackDuration() const;

    void setQueryId( const QString &id );
    void setTrackName( const QString &name );
    void setArtistName( const QString &name );
    void setAlbumName( const QString &name );
    void setTrackDuration( const qulonglong duration );

    QVariantMap toJson();
    void fromJson( const QVariantMap &json );

private:
    explicit Query();

    QueryImplPtr m_impl;
};
//------------------------------------------------------------------------------


#endif // SETPLAYLISTREVISIONCOMMAND_H
