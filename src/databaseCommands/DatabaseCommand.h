/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef DATABASECOMMAND_H
#define DATABASECOMMAND_H

#include "DatabaseCommandsTypedefs.h"
#include "MsgTypedefs.h"

#include <QVariantMap>

class DatabaseCommand
{
public:
    enum DatabaseCommandType { UnknownCommand,
                               AddFilesCommand,
                               DeleteFilesCommand,
                               CreatePlaylistCommand,
                               RenamePlaylistCommand,
                               SetPlaylistRevisionCommand,
                               DeletePlaylistCommand,
                               SocialActionCommand,
                               LogPlaybackCommand
                             };

    explicit DatabaseCommand();

    virtual const DatabaseCommandType type() const = 0;

    const QString &guid() const;
    void setGuid( const QString &guid ) const;

    virtual bool jsonToDatabaseCommand( const QVariantMap &json );
    virtual const QVariantMap databaseCommandToJson() const;

private:
    virtual const QString commandName() const = 0;

    DatabaseCommandImplPtr m_impl;
};

#endif // DATABASECOMMAND_H
