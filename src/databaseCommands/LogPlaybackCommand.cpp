/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "LogPlaybackCommand.h"

#include "LogPlaybackCommandStrings.h"

//------------------------------------------------------------------------------
class LogPlaybackCommandImpl
{
public:
    LogPlaybackCommandImpl()
    {
    }

    QString artistName;
    QString trackName;
    quint64 timestamp;
    quint32 secondsPlayed;
    LogPlaybackCommand::ActionType actionType;
};
//------------------------------------------------------------------------------

LogPlaybackCommand::LogPlaybackCommand()
    : DatabaseCommand()
    , m_impl( LogPlaybackCommandImplPtr( new LogPlaybackCommandImpl ) )
{
}

const LogPlaybackCommand::ActionType
LogPlaybackCommand::convertActionTypeIdToActionType( const quint8 &actionTypeId ) const
{
    LogPlaybackCommand::ActionType resultActionType;

    switch( actionTypeId )
    {
    case LogPlaybackCommand::StartPlaying:
    case LogPlaybackCommand::FinishedPlaying:
    {
        resultActionType = LogPlaybackCommand::ActionType( actionTypeId );
        break;
    }
    default:
    {
        resultActionType = LogPlaybackCommand::UnknownActionType;
        break;
    }
    }

    return resultActionType;
}

LogPlaybackCommandPtr
LogPlaybackCommand::create()
{
    return LogPlaybackCommandPtr( new LogPlaybackCommand );
}

const DatabaseCommand::DatabaseCommandType
LogPlaybackCommand::type() const
{
    return DatabaseCommand::LogPlaybackCommand;
}

const QString
LogPlaybackCommand::commandName() const
{
    return ProtocolStr::LogPlaybackCommandObj::valCommandName();
}

const QString&
LogPlaybackCommand::artistName() const
{
    return m_impl->artistName;
}

void
LogPlaybackCommand::setArtistName( const QString &artistName )
{
    m_impl->artistName = artistName;
}

const QString&
LogPlaybackCommand::trackName() const
{
    return m_impl->trackName;
}

void
LogPlaybackCommand::setTrackName( const QString &trackName )
{
    m_impl->trackName = trackName;
}

const LogPlaybackCommand::ActionType&
LogPlaybackCommand::actionType() const
{
    return m_impl->actionType;
}

void
LogPlaybackCommand::setActionType( const LogPlaybackCommand::ActionType &actionType )
{
    m_impl->actionType = actionType;
}

quint64
LogPlaybackCommand::timestamp() const
{
    return m_impl->timestamp;
}

void
LogPlaybackCommand::setTimestamp( const quint64 &timestamp )
{
    m_impl->timestamp = timestamp;
}

quint32
LogPlaybackCommand::secondsPlayed() const
{
    return m_impl->secondsPlayed;
}

void
LogPlaybackCommand::setSecondsPlayed( const quint32 &secondsPlayed )
{
    m_impl->secondsPlayed = secondsPlayed;
}

bool
LogPlaybackCommand::jsonToDatabaseCommand( const QVariantMap &json )
{
    if( !DatabaseCommand::jsonToDatabaseCommand( json ) )
        return false;

    LogPlaybackCommand::ActionType actionType;
    actionType = convertActionTypeIdToActionType( json[ProtocolStr::LogPlaybackCommandObj::actionType()].toUInt() );

    if( actionType == LogPlaybackCommand::UnknownActionType )
        return false;

    setArtistName( json[ProtocolStr::LogPlaybackCommandObj::artistName()].toString() );
    setTrackName( json[ProtocolStr::LogPlaybackCommandObj::trackName()].toString() );
    setActionType( actionType );
    setTimestamp( json[ProtocolStr::LogPlaybackCommandObj::timestamp()].toLongLong() );
    setSecondsPlayed( json[ProtocolStr::LogPlaybackCommandObj::secondsPlayed()].toUInt() );

    return true;
}

const QVariantMap
LogPlaybackCommand::databaseCommandToJson() const
{
    QVariantMap json = DatabaseCommand::databaseCommandToJson();

    if( actionType() == LogPlaybackCommand::UnknownActionType )
        return json;

    json[ProtocolStr::LogPlaybackCommandObj::artistName()] = artistName();
    json[ProtocolStr::LogPlaybackCommandObj::trackName()] = trackName();
    json[ProtocolStr::LogPlaybackCommandObj::actionType()] = actionType();
    json[ProtocolStr::LogPlaybackCommandObj::timestamp()] = timestamp();
    json[ProtocolStr::LogPlaybackCommandObj::secondsPlayed()] = secondsPlayed();

    return json;
}
