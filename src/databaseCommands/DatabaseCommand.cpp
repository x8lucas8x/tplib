/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "DatabaseCommand.h"

#include "DatabaseCommandStrings.h"

//------------------------------------------------------------------------------
class DatabaseCommandImpl
{
public:
    DatabaseCommandImpl()
    {
    }

    QString guid;
};
//------------------------------------------------------------------------------

DatabaseCommand::DatabaseCommand()
    : m_impl( DatabaseCommandImplPtr( new DatabaseCommandImpl ) )
{
}

const QString&
DatabaseCommand::guid() const
{
    return m_impl->guid;
}

void
DatabaseCommand::setGuid( const QString &guid ) const
{
    m_impl->guid = guid;
}

bool
DatabaseCommand::jsonToDatabaseCommand( const QVariantMap &json )
{
    if( json.isEmpty() )
        return false;

    if( json[ProtocolStr::DatabaseCommandObj::commandName()].toString() != commandName() )
        return false;

    setGuid( json[ProtocolStr::DatabaseCommandObj::guid()].toString() );

    return true;
}

const QVariantMap
DatabaseCommand::databaseCommandToJson() const
{
    QVariantMap json;

    json[ProtocolStr::DatabaseCommandObj::commandName()] = commandName();
    json[ProtocolStr::DatabaseCommandObj::guid()] = guid();

    return json;
}
