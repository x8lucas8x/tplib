/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef LOGPLAYBACKCOMMAND_H
#define LOGPLAYBACKCOMMAND_H

#include "DatabaseCommandsTypedefs.h"
#include "DatabaseCommand.h"

class LogPlaybackCommand : public DatabaseCommand
{
public:
    enum ActionType { UnknownActionType = 0, StartPlaying, FinishedPlaying };

    static LogPlaybackCommandPtr create();

    virtual const DatabaseCommandType type() const;

    const QString &artistName() const;
    void setArtistName( const QString &artistName );

    const QString &trackName() const;
    void setTrackName( const QString &trackName );

    const ActionType &actionType() const;
    void setActionType( const ActionType &actionType );

    quint64 timestamp() const;
    void setTimestamp( const quint64 &timestamp );

    quint32 secondsPlayed() const;
    void setSecondsPlayed( const quint32 &secondsPlayed );

    virtual bool jsonToDatabaseCommand( const QVariantMap &json );
    virtual const QVariantMap databaseCommandToJson() const;

private:
    explicit LogPlaybackCommand();
    virtual const QString commandName() const;
    const LogPlaybackCommand::ActionType convertActionTypeIdToActionType( const quint8 &actionTypeId ) const;

    LogPlaybackCommandImplPtr m_impl;
};

#endif // LOGPLAYBACKCOMMAND_H
