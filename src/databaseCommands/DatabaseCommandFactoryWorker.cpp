/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "DatabaseCommandFactoryWorker.h"

#include "DatabaseCommandUtilityFunctions.h"
#include "DatabaseCommandStrings.h"
#include "AddFilesCommand.h"
#include "DeleteFilesCommand.h"
#include "CreatePlaylistCommand.h"
#include "DeletePlaylistCommand.h"
#include "RenamePlaylistCommand.h"
#include "LogPlaybackCommand.h"
#include "SocialActionCommand.h"
#include "SetPlaylistRevisionCommand.h"
#include "Track.h"
#include "Playlist.h"

#include <QMutex>
#include <QMutexLocker>
#include <QtConcurrentRun>
#include <QtConcurrentMap>
#include <QFutureSynchronizer>

//------------------------------------------------------------------------------
class DatabaseCommandFactoryWorkerImpl
{
public:
    DatabaseCommandFactoryWorkerImpl()
    {
    }

    QList<AddFilesCommandPtr> addFilesCommands;
    QList<DeleteFilesCommandPtr> deleteFilesCommands;
    QList<CreatePlaylistCommandPtr> createPlaylistCommands;
    QList<RenamePlaylistCommandPtr> renamePlaylistCommands;
    QList<DeletePlaylistCommandPtr> deletePlaylistCommands;
    QList<SetPlaylistRevisionCommandPtr> setPlaylistRevisionCommands;
    QList<SocialActionCommandPtr> socialActionCommands;
    QList<LogPlaybackCommandPtr> logPlaybackCommands;

    QMutex addFilesCommandsListMutex;
    QMutex deleteFilesCommandsListMutex;
    QMutex createPlaylistCommandsListMutex;
    QMutex renamePlaylistCommandsListMutex;
    QMutex deletePlaylistCommandsListMutex;
    QMutex setPlaylistRevisionCommandsListMutex;
    QMutex socialActionCommandsListMutex;
    QMutex logPlaybackCommandsListMutex;
};
//------------------------------------------------------------------------------

DatabaseCommandFactoryWorker::DatabaseCommandFactoryWorker()
    : QObject()
    , m_impl( DatabaseCommandFactoryWorkerImplPtr( new DatabaseCommandFactoryWorkerImpl ) )
{
}

void
DatabaseCommandFactoryWorker::doWork( const MsgListPtr msgs )
{
    QList<QVariantMap> commandJsons =
            QtConcurrent::blockingMapped( msgs, DatabaseCommandUtils::msgToDatabaseCommandJson );
    const QString &lastGuid = commandJsons.last().value( ProtocolStr::DatabaseCommandObj::guid() ).toString();

    foreach( const QVariantMap &json, commandJsons )
    {
        createCommand( json );
    }

    QFutureSynchronizer<void> synchronizer;
    synchronizer.addFuture( QtConcurrent::run( this, &DatabaseCommandFactoryWorker::processFilesCommands ) );
    synchronizer.addFuture( QtConcurrent::run( this, &DatabaseCommandFactoryWorker::processPlaylistCommands ) );
    synchronizer.addFuture( QtConcurrent::run( this, &DatabaseCommandFactoryWorker::processGeneralCommands ) );
    synchronizer.waitForFinished();

    emit finished( lastGuid );
}

void
DatabaseCommandFactoryWorker::createCommand( const QVariantMap &json )
{
    DatabaseCommand::DatabaseCommandType type;
    bool isJsonCompatible = false;
    const QString commandName = json.value( ProtocolStr::DatabaseCommandObj::commandName() ).toString();
    type = DatabaseCommandUtils::databaseCommandNameToType( commandName );

    switch( type )
    {
    case DatabaseCommand::AddFilesCommand:
    {
        QMutexLocker( &m_impl->addFilesCommandsListMutex );
        AddFilesCommandPtr command = AddFilesCommand::create();
        isJsonCompatible = command->jsonToDatabaseCommand( json );
        m_impl->addFilesCommands.append( command );
    }
    case DatabaseCommand::DeleteFilesCommand:
    {
        QMutexLocker( &m_impl->deleteFilesCommandsListMutex );
        DeleteFilesCommandPtr command = DeleteFilesCommand::create();
        isJsonCompatible = command->jsonToDatabaseCommand( json );
        m_impl->deleteFilesCommands.append( command );
    }
    case DatabaseCommand::CreatePlaylistCommand:
    {
        QMutexLocker( &m_impl->createPlaylistCommandsListMutex );
        CreatePlaylistCommandPtr command = CreatePlaylistCommand::create();
        isJsonCompatible = command->jsonToDatabaseCommand( json );
        m_impl->createPlaylistCommands.append( command );
    }
    case DatabaseCommand::RenamePlaylistCommand:
    {
        QMutexLocker( &m_impl->renamePlaylistCommandsListMutex );
        RenamePlaylistCommandPtr command = RenamePlaylistCommand::create();
        isJsonCompatible = command->jsonToDatabaseCommand( json );
        m_impl->renamePlaylistCommands.append( command );
    }
    case DatabaseCommand::DeletePlaylistCommand:
    {
        QMutexLocker( &m_impl->deletePlaylistCommandsListMutex );
        DeletePlaylistCommandPtr command = DeletePlaylistCommand::create();
        isJsonCompatible = command->jsonToDatabaseCommand( json );
        m_impl->deletePlaylistCommands.append( command );
    }
    case DatabaseCommand::SetPlaylistRevisionCommand:
    {
        QMutexLocker( &m_impl->setPlaylistRevisionCommandsListMutex );
        SetPlaylistRevisionCommandPtr command = SetPlaylistRevisionCommand::create();
        isJsonCompatible = command->jsonToDatabaseCommand( json );
        m_impl->setPlaylistRevisionCommands.append( command );
    }
    case DatabaseCommand::LogPlaybackCommand:
    {
        QMutexLocker( &m_impl->logPlaybackCommandsListMutex );
        LogPlaybackCommandPtr command = LogPlaybackCommand::create();
        isJsonCompatible = command->jsonToDatabaseCommand( json );
        m_impl->logPlaybackCommands.append( command );
    }
    case DatabaseCommand::SocialActionCommand:
    {
        QMutexLocker( &m_impl->socialActionCommandsListMutex );
        SocialActionCommandPtr command = SocialActionCommand::create();
        isJsonCompatible = command->jsonToDatabaseCommand( json );
        m_impl->socialActionCommands.append( command );
    }
    case DatabaseCommand::UnknownCommand:
    default:
    {
        // This should never happen, otherwise TPLib must crash.
        Q_ASSERT_X(false,
                   Q_FUNC_INFO,
                   "The msg passed as argument doesn't have a valid database command.");
    }
    }

    // Crash if the wrong database command was instantiated.
    Q_ASSERT_X(isJsonCompatible == true,
               Q_FUNC_INFO,
               "The wrong database command was instantiated.");
}

void
DatabaseCommandFactoryWorker::processFilesCommands()
{
    QMap<qulonglong, TPCoreApi::TrackPtr> tracksToAdd;
    QList<qulonglong> trackIdsToRemove;

    foreach( const AddFilesCommandPtr &command, m_impl->addFilesCommands )
    {
        foreach( const TrackPtr &databaseTrack, command->tracks() )
        {
            TPCoreApi::TrackPtr track = databaseTrack->toTrack();
            tracksToAdd.insert( track->id(), track );
        }
    }

    foreach( const DeleteFilesCommandPtr &command, m_impl->deleteFilesCommands )
    {
        foreach( const qulonglong &fileId, command->fileIds() )
        {
            if( tracksToAdd.contains( fileId ) )
                tracksToAdd.remove( fileId );
            else
                trackIdsToRemove.append( fileId );
        }
    }

    emit tracksAdded( tracksToAdd.values() );
    emit tracksRemoved( trackIdsToRemove );
}

void
DatabaseCommandFactoryWorker::processPlaylistCommands()
{
    QMap<QString, TPCoreApi::PlaylistPtr> playlistsToCreate;
    QList<QString> playlistsToDelete;


    foreach( const CreatePlaylistCommandPtr &command, m_impl->createPlaylistCommands )
    {
        TPCoreApi::PlaylistPtr playlist = command->playlist()->toPlaylist();
        playlistsToCreate.insert( playlist->guid(), playlist );
    }

    foreach( const RenamePlaylistCommandPtr &command, m_impl->renamePlaylistCommands )
    {
        if( playlistsToCreate.contains( command->playlistGuid() ) )
            playlistsToCreate.value( command->playlistGuid() )->setTitle( command->playlistNewTitle() );
        else
            emit playlistRenamed( command->playlistGuid(), command->playlistNewTitle() );
    }

    foreach( const SetPlaylistRevisionCommandPtr &command, m_impl->setPlaylistRevisionCommands )
    {
        TPCoreApi::PlaylistRevisionPtr revision = command->toPlaylistRevision();

        if( playlistsToCreate.contains( command->playlistGuid() ) )
            playlistsToCreate.value( command->playlistGuid() )->setCurrentRevisionGuid( revision->revisionGuid() );
        else
            emit playlistChanged( command->playlistGuid(), revision );
    }

    foreach( const DeletePlaylistCommandPtr &command, m_impl->deletePlaylistCommands )
    {
        if( playlistsToCreate.contains( command->playlistGuid() ) )
            playlistsToCreate.remove( command->playlistGuid() );
        else
            playlistsToDelete.append( command->playlistGuid() );
    }

    emit playlistsCreated( playlistsToCreate.values() );
    emit playlistsDeleted( playlistsToDelete );
}

void
DatabaseCommandFactoryWorker::processGeneralCommands()
{
    QList<SocialActionCommandPtr> socialActionCommands;
    QList<LogPlaybackCommandPtr> logPlaybackCommands;

    foreach( const SocialActionCommandPtr &socialCommand, m_impl->socialActionCommands )
    {
        if( socialCommand->actionType() == SocialActionCommand::Love )
            emit trackLoved( socialCommand->artistName(), socialCommand->trackName() );
        else if( socialCommand->actionType() == SocialActionCommand::Unlove )
            emit trackUnloved( socialCommand->artistName(), socialCommand->trackName() );
        else
            Q_ASSERT_X(false, Q_FUNC_INFO, "A social command with an unknown action type was found.");
    }

    foreach( const LogPlaybackCommandPtr &logPlaybackCommand, m_impl->logPlaybackCommands )
    {
        if( logPlaybackCommand->actionType() == LogPlaybackCommand::StartPlaying )
            emit playbackStarted( logPlaybackCommand->artistName(), logPlaybackCommand->trackName() );
        else if( logPlaybackCommand->actionType() == LogPlaybackCommand::FinishedPlaying )
            emit playbackFinished( logPlaybackCommand->artistName(),
                                   logPlaybackCommand->trackName(),
                                   logPlaybackCommand->secondsPlayed() );
        else
            Q_ASSERT_X(false, Q_FUNC_INFO, "A log playback command with an unknown action type was found.");
    }
}
