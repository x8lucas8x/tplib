/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "RenamePlaylistCommand.h"

#include "RenamePlaylistCommandStrings.h"

//------------------------------------------------------------------------------
class RenamePlaylistCommandImpl
{
public:
    RenamePlaylistCommandImpl()
    {
    }

    QString playlistNewTitle;
};
//------------------------------------------------------------------------------

RenamePlaylistCommand::RenamePlaylistCommand()
    : PlaylistDatabaseCommand()
    , m_impl( RenamePlaylistCommandImplPtr( new RenamePlaylistCommandImpl ) )
{
}


RenamePlaylistCommandPtr
RenamePlaylistCommand::create()
{
    return RenamePlaylistCommandPtr( new RenamePlaylistCommand );
}

const DatabaseCommand::DatabaseCommandType
RenamePlaylistCommand::type() const
{
    return DatabaseCommand::RenamePlaylistCommand;
}

const QString
RenamePlaylistCommand::commandName() const
{
    return ProtocolStr::RenamePlaylistCommandObj::valCommandName();
}

const QString&
RenamePlaylistCommand::playlistNewTitle() const
{
    return m_impl->playlistNewTitle;
}

void
RenamePlaylistCommand::setPlaylistNewTitle( const QString &playlistNewTitle )
{
    m_impl->playlistNewTitle = playlistNewTitle;
}

bool
RenamePlaylistCommand::jsonToDatabaseCommand( const QVariantMap &json )
{
    if( !PlaylistDatabaseCommand::jsonToDatabaseCommand( json ) )
        return false;

    setPlaylistNewTitle( json[ProtocolStr::RenamePlaylistCommandObj::playlistNewTitle()].toString() );

    return true;
}

const QVariantMap
RenamePlaylistCommand::databaseCommandToJson() const
{
    QVariantMap json = PlaylistDatabaseCommand::databaseCommandToJson();

    json[ProtocolStr::RenamePlaylistCommandObj::playlistNewTitle()] = playlistNewTitle();

    return json;
}
