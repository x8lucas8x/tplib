/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "MsgProcessingUtilityFunctions.h"

#include "DatabaseCommand.h"

#include <qjson/parser.h>
#include <qjson/serializer.h>

bool
MsgProcessingUtils::isValidJson( const QByteArray &inputBuffer )
{
    QJson::Parser parser;
    bool ok;
    parser.parse( inputBuffer, &ok );

    return ok;
}

const QVariantMap
MsgProcessingUtils::parseJson( const QByteArray &inputBuffer )
{
    QJson::Parser parser;
    return parser.parse( inputBuffer ).toMap();
}

const QByteArray
MsgProcessingUtils::serializeJson( const QVariantMap &json )
{
    QJson::Serializer serializer;
    return serializer.serialize( json );
}
