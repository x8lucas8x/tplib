/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef MSGPROCESSINGUTILITYFUNCTIONS_H
#define MSGPROCESSINGUTILITYFUNCTIONS_H

#include <QVariantMap>
#include <QByteArray>

namespace MsgProcessingUtils
{
    // Verify if a buffer contains a valid json object.
    bool isValidJson( const QByteArray &inputBuffer );
    // Parse a buffer and put the read json object, if any, in result argument.
    const QVariantMap parseJson( const QByteArray &inputBuffer );
    // Serialize a json object, if any.
    const QByteArray serializeJson( const QVariantMap &json );
}

#endif // MSGPROCESSINGUTILITYFUNCTIONS_H
