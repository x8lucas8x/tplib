/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef NETWORKUTILITYFUNCTIONS_H
#define NETWORKUTILITYFUNCTIONS_H

#include "ServerNodeTypedefs.h"

class QHostAddress;

namespace NetworkUtils
{
    // Verify if ip is part of the local network.
    bool isIPWhitelisted( const QHostAddress &ip );

    // Return a TCP socket if the address and port were valid.
    QTcpSocketPtr createTcpConnection( const QHostAddress& address, const quint16 port );
}

#endif // NETWORKUTILITYFUNCTIONS_H
