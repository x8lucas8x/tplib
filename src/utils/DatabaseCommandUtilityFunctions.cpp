/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "DatabaseCommandUtilityFunctions.h"

#include "Msg.h"
#include "MsgProcessingUtilityFunctions.h"
#include "AddFilesCommandStrings.h"
#include "DeleteFilesCommandStrings.h"
#include "CreatePlaylistCommandStrings.h"
#include "DeletePlaylistCommandStrings.h"
#include "RenamePlaylistCommandStrings.h"
#include "LogPlaybackCommandStrings.h"
#include "SocialActionCommandStrings.h"
#include "SetPlaylistRevisionCommandStrings.h"
#include "UnsupportedCommandStrings.h"

const DatabaseCommand::DatabaseCommandType
DatabaseCommandUtils::databaseCommandNameToType( const QString &commandName )
{
    if( commandName.isEmpty() )
    {
        // This should never happen, otherwise TPLib must crash.
        Q_ASSERT_X(false,
                   Q_FUNC_INFO,
                   "There is not command name field available in the given json.");

        return DatabaseCommand::UnknownCommand;
    }

    if( commandName == ProtocolStr::AddFilesCommandObj::valCommandName() )
    {
        return DatabaseCommand::AddFilesCommand;
    }
    else if( commandName == ProtocolStr::DeleteFilesCommandObj::valCommandName() )
    {
        return DatabaseCommand::DeleteFilesCommand;
    }
    else if( commandName == ProtocolStr::CreatePlaylistCommandObj::valCommandName() )
    {
        return DatabaseCommand::CreatePlaylistCommand;
    }
    else if( commandName == ProtocolStr::DeletePlaylistCommandObj::valCommandName() )
    {
        return DatabaseCommand::DeletePlaylistCommand;
    }
    else if( commandName == ProtocolStr::RenamePlaylistCommandObj::valCommandName() )
    {
        return DatabaseCommand::RenamePlaylistCommand;
    }
    else if( commandName == ProtocolStr::SetPlaylistRevisionCommandObj::valCommandName() )
    {
        return DatabaseCommand::SetPlaylistRevisionCommand;
    }
    else if( commandName == ProtocolStr::LogPlaybackCommandObj::valCommandName() )
    {
        return DatabaseCommand::LogPlaybackCommand;
    }
    else if( commandName == ProtocolStr::SocialActionCommandObj::valCommandName() )
    {
        return DatabaseCommand::SocialActionCommand;
    }
    else if( isDatabaseCommandUnsupported( commandName ) )
    {
        // Ignore these commands, since we do not support them in this tomahawk
        // protocol implementation.
    }
    else
    {
        // This should never happen, otherwise TPLib must crash.
        Q_ASSERT_X(false,
                   Q_FUNC_INFO,
                   "Unkown database command name.");
    }
}

bool
DatabaseCommandUtils::isDatabaseCommandUnsupported( const QString &commandName )
{
    if( commandName == ProtocolStr::UnsupportedCommands::createDynamicPlaylistValCommandName() ||
        commandName == ProtocolStr::UnsupportedCommands::deleteDynamicPlaylistValCommandName() ||
        commandName == ProtocolStr::UnsupportedCommands::setCollectionAttributesValCommandName() ||
        commandName == ProtocolStr::UnsupportedCommands::setDynamicPlaylistRevisionValCommandName() ||
        commandName == ProtocolStr::UnsupportedCommands::setTrackAttributesValCommandName() )
    {
        return false;
    }

    return true;
}

QVariantMap
DatabaseCommandUtils::msgToDatabaseCommandJson( const MsgPtr &msg )
{
    if( !msg->hasFlag( Msg::DBOP ) )
    {
        // This should never happen, otherwise TPLib must crash.
        Q_ASSERT_X(false,
                   Q_FUNC_INFO,
                   "The msg passed as argument doesn't have a DBOP flag.");

        return QVariantMap();
    }

    return MsgProcessingUtils::parseJson( msg->payload() );
}
