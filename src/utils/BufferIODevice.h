/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef BUFFERIODEVICE_H
#define BUFFERIODEVICE_H

#include "UtilsTypedefs.h"

#include <QIODevice>

class BufferIODevice : public QIODevice
{
Q_OBJECT

public:
    static quint32 blockSize();

    explicit BufferIODevice( const quint32 &size = 0, QObject* parent = 0 );

    void addData( const QByteArray& ba );
    void clear();
    void inputComplete( const QString& errorString = "" );
    int maxBlocks() const;
    int nextEmptyBlock() const;
    bool isBlockEmpty( int block ) const;

    // QIODevice Methods
    virtual bool open( OpenMode mode );
    virtual void close();
    virtual bool seek( qint64 pos );
    virtual qint64 bytesAvailable() const;
    virtual qint64 size() const;
    virtual bool atEnd() const;
    virtual qint64 pos() const;
    virtual bool isSequential() const { return false; }

signals:
    void blockRequest( int block );

protected:
    virtual qint64 readData( char* data, qint64 maxSize );

    // Call addData instead.
    virtual qint64 writeData( const char* data, qint64 maxSize );

private:
    const quint32 blockForPosition( qint64 pos ) const;
    const quint32 offsetForPosition( qint64 pos ) const;
    QByteArray getData( qint64 pos, qint64 size );

    BufferIODeviceImplPtr m_impl;
};

#endif // BUFFERIODEVICE_H
