/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef GLOBALUTILITYMACROS_H
#define GLOBALUTILITYMACROS_H

/*
   Some classes do not permit copies/instantiation to be made of an object. These
   classes contains a private copy constructor, default constructor and assignment
   operator to disable copying/instantiation (the compiler gives an error message).
*/
#define Q_DISABLE_INSTANTIATION(Class) \
    private: \
        explicit Class(); \
        Q_DISABLE_COPY(Class)

#endif // GLOBALUTILITYMACROS_H
