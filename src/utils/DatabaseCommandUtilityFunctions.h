/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef DATABASECOMMANDUTILITYFUNCTIONS_H
#define DATABASECOMMANDUTILITYFUNCTIONS_H

#include "MsgTypedefs.h"
#include "DatabaseCommand.h"

namespace DatabaseCommandUtils
{
    // Returns true if a given database command name isn't supported by TPLib.
    bool isDatabaseCommandUnsupported( const QString &commandName );
    // Returns the database command type of a given database command name.
    const DatabaseCommand::DatabaseCommandType databaseCommandNameToType( const QString &commandName );
    // Converts msg to json.
    QVariantMap msgToDatabaseCommandJson( const MsgPtr &msg );
}

#endif // DATABASECOMMANDUTILITYFUNCTIONS_H
