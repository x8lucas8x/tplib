/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "BufferIODevice.h"

#include <QMutexLocker>

//------------------------------------------------------------------------------
class BufferIODeviceImpl
{
public:
    BufferIODeviceImpl( const quint32 &_size )
        : size( _size )
        , bytesWritten( 0 )
        , position( 0 )
    {
    }

    QList<QByteArray> buffer;
    quint32 size;
    quint32 bytesWritten;
    quint32 position;
    QMutex mutex;
};
//------------------------------------------------------------------------------

BufferIODevice::BufferIODevice( const quint32 &size, QObject* parent )
    : QIODevice( parent )
    , m_impl( BufferIODeviceImplPtr( new BufferIODeviceImpl( size ) ) )
{
}

bool
BufferIODevice::open( OpenMode mode )
{
    Q_UNUSED(mode);
    QMutexLocker lock( &m_impl->mutex );
    return QIODevice::open( QIODevice::ReadOnly | QIODevice::Unbuffered );
}

void
BufferIODevice::close()
{
    QMutexLocker lock( &m_impl->mutex );
    QIODevice::close();
}

bool
BufferIODevice::seek( qint64 pos )
{
    if ( pos >= m_impl->size )
        return false;

    int block = blockForPosition( pos );
    if ( isBlockEmpty( block ) )
        emit blockRequest( block );

    m_impl->position = pos;

    return true;
}

void
BufferIODevice::inputComplete( const QString& errorString )
{
    setErrorString( errorString );
    m_impl->size = m_impl->bytesWritten;
    emit readChannelFinished();
}

void
BufferIODevice::addData( const QByteArray& ba )
{
    {
        QMutexLocker lock( &m_impl->mutex );
        m_impl->buffer.append( ba );
    }

    // If this was the last block of the transfer, check if we need to fill up gaps.
    if ( ba.count() == maxBlocks() )
    {
        if ( nextEmptyBlock() >= 0 )
        {
            emit blockRequest( nextEmptyBlock() );
        }
    }

    m_impl->bytesWritten += ba.count();
    emit bytesWritten( ba.count() );
    emit readyRead();
}

qint64
BufferIODevice::bytesAvailable() const
{
    return m_impl->size - m_impl->position;
}

qint64
BufferIODevice::readData( char* data, qint64 maxSize )
{
    if ( atEnd() )
        return 0;

    QByteArray ba;
    ba.append( getData( m_impl->position, maxSize ) );
    m_impl->position += ba.count();
    memcpy( data, ba.data(), ba.count() );

    return ba.count();
}

qint64
BufferIODevice::writeData( const char* data, qint64 maxSize )
{
    Q_UNUSED( data );
    Q_UNUSED( maxSize );
    Q_ASSERT( false );
    return 0;
}

qint64
BufferIODevice::size() const
{
    return m_impl->size;
}

bool
BufferIODevice::atEnd() const
{
    return m_impl->size <= m_impl->position;
}

qint64
BufferIODevice::pos() const
{
    return m_impl->position;
}

void
BufferIODevice::clear()
{
    QMutexLocker lock( &m_impl->mutex );
    m_impl->position = 0;
    m_impl->buffer.clear();
}

quint32
BufferIODevice::blockSize()
{
    return 4096;
}

const quint32 BufferIODevice::blockForPosition( qint64 pos ) const
{
    // 0 / 4096 -> block 0
    // 4095 / 4096 -> block 0
    // 4096 / 4096 -> block 1
    return pos / blockSize();
}

const quint32 BufferIODevice::offsetForPosition( qint64 pos ) const
{
    // 0 % 4096 -> offset 0
    // 4095 % 4096 -> offset 4095
    // 4096 % 4096 -> offset 0
    return pos % blockSize();
}

int
BufferIODevice::nextEmptyBlock() const
{
    int i = m_impl->buffer.count();
    if ( i == maxBlocks() )
        return -1;

    return i;
}

int
BufferIODevice::maxBlocks() const
{
    int i = m_impl->size / blockSize();

    if ( ( m_impl->size % blockSize() ) > 0 )
        i++;

    return i;
}

bool
BufferIODevice::isBlockEmpty( int block ) const
{
    if ( block >= m_impl->buffer.count() )
        return true;

    return m_impl->buffer.at( block ).isEmpty();
}

QByteArray
BufferIODevice::getData( qint64 pos, qint64 size )
{
    QByteArray ba;
    int block = blockForPosition( pos );
    int offset = offsetForPosition( pos );

    QMutexLocker lock( &m_impl->mutex );
    while( ba.count() < size )
    {
        if ( block > maxBlocks() )
            break;

        if ( isBlockEmpty( block ) )
            break;

        ba.append( m_impl->buffer.at( block++ ).mid( offset ) );
    }

    return ba.left( size );
}
