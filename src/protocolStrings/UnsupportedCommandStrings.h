/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef UNSUPPORTEDCOMMANDSTRINGS_H
#define UNSUPPORTEDCOMMANDSTRINGS_H

#include "GlobalUtilityMacros.h"

namespace ProtocolStr
{

struct UnsupportedCommands
{
    static const char *createDynamicPlaylistValCommandName() { return "createdynamicplaylist"; }
    static const char *deleteDynamicPlaylistValCommandName() { return "deletedynamicplaylist"; }
    static const char *setDynamicPlaylistRevisionValCommandName() { return "setdynamicplaylistrevision"; }
    static const char *setCollectionAttributesValCommandName() { return "setcollectionattributes"; }
    static const char *setTrackAttributesValCommandName() { return "settrackattributes"; }

    Q_DISABLE_INSTANTIATION(UnsupportedCommands);
};

}

#endif // UNSUPPORTEDCOMMANDSTRINGS_H
