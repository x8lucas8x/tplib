/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef CREATEPLAYLISTCOMMANDSTRINGS_H
#define CREATEPLAYLISTCOMMANDSTRINGS_H

#include "DatabaseCommandStrings.h"
#include "GlobalUtilityMacros.h"

namespace ProtocolStr
{

struct CreatePlaylistCommandObj : public DatabaseCommandObj
{
    static const char *valCommandName() { return "createplaylist"; }
    static const char *playlistObj() { return "playlist"; }

    struct PlaylistObj
    {

        static const char *info() { return "info"; }
        static const char *creator() { return "creator"; }
        static const char *createdOn() { return "createdon"; }
        static const char *title() { return "title"; }
        static const char *currentRevision() { return "currentrevision"; }
        static const char *isShared() { return "shared"; }
        static const char *playlistGuid() { return "guid"; }

        Q_DISABLE_INSTANTIATION(PlaylistObj);
    };

    Q_DISABLE_INSTANTIATION(CreatePlaylistCommandObj);
};

}

#endif // CREATEPLAYLISTCOMMANDSTRINGS_H
