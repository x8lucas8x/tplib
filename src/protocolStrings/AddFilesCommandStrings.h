/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef ADDFILESCOMMANDSTRINGS_H
#define ADDFILESCOMMANDSTRINGS_H

#include "DatabaseCommandStrings.h"
#include "GlobalUtilityMacros.h"

namespace ProtocolStr
{

struct AddFilesCommandObj : public DatabaseCommandObj
{
    static const char *valCommandName() { return "addfiles"; }
    static const char *filesArray() { return "files"; }

    struct FilesObj
    {
        static const char *trackId() { return "id"; }
        static const char *trackUrl() { return "url"; }
        static const char *artistName() { return "artist"; }
        static const char *albumName() { return "album"; }
        static const char *trackName() { return "track"; }
        static const char *trackMimeType() { return "mimetype"; }
        static const char *trackHash() { return "hash"; }
        static const char *trackYear() { return "year"; }
        static const char *trackAlbumPosition() { return "albumpos"; }
        static const char *trackModifiedTime() { return "mtime"; }
        static const char *trackDuration() { return "duration"; }
        static const char *trackBitRate() { return "bitrate"; }
        static const char *trackSize() { return "size"; }

        Q_DISABLE_INSTANTIATION(FilesObj);
    };

    Q_DISABLE_INSTANTIATION(AddFilesCommandObj);
};

}

#endif // ADDFILESCOMMANDSTRINGS_H
