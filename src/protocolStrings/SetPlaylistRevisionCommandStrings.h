/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef SETPLAYLISTREVISIONCOMMANDSTRINGS_H
#define SETPLAYLISTREVISIONCOMMANDSTRINGS_H

#include "PlaylistDatabaseCommandStrings.h"
#include "GlobalUtilityMacros.h"

namespace ProtocolStr
{

struct SetPlaylistRevisionCommandObj : public PlaylistDatabaseCommandObj
{
    static const char *valCommandName() { return "setplaylistrevision"; }
    static const char *oldRevision() { return "oldrev"; }
    static const char *newRevision() { return "newrev"; }
    static const char *addedEntriesArray() { return "addedentries"; }
    static const char *orderedGuidsArray() { return "orderedguids"; }

    struct AddedEntryObj
    {

        static const char *duration() { return "duration"; }
        static const char *lastModified() { return "lastmodified"; }
        static const char *entryGuid() { return "guid"; }
        static const char *annotation() { return "annotation"; }
        static const char *queryObj() { return "query"; }

        struct QueryObj
        {

            static const char *albumName() { return "album"; }
            static const char *trackName() { return "track"; }
            static const char *artistName() { return "artist"; }
            static const char *duration() { return "duration"; }
            static const char *queryId() { return "qid"; }

            Q_DISABLE_INSTANTIATION(QueryObj);
        };

        Q_DISABLE_INSTANTIATION(AddedEntryObj);
    };

    Q_DISABLE_INSTANTIATION(SetPlaylistRevisionCommandObj);
};

}

#endif // SETPLAYLISTREVISIONCOMMANDSTRINGS_H
