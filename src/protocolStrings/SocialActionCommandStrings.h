/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef SOCIALACTIONCOMMANDSTRINGS_H
#define SOCIALACTIONCOMMANDSTRINGS_H

#include "DatabaseCommandStrings.h"
#include "GlobalUtilityMacros.h"

namespace ProtocolStr
{

struct SocialActionCommandObj : public DatabaseCommandObj
{
    static const char *valCommandName() { return "socialaction"; }
    static const char *actionType() { return "action"; }
    static const char *actionComment() { return "comment"; }
    static const char *artistName() { return "artist"; }
    static const char *trackName() { return "track"; }
    static const char *timestamp() { return "timestamp"; }

    Q_DISABLE_INSTANTIATION(SocialActionCommandObj);
};

}

#endif // SOCIALACTIONCOMMANDSTRINGS_H
