/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef MSG_H
#define MSG_H

#include "MsgTypedefs.h"

#include <QObject>

class Msg : public QObject
{
    Q_OBJECT
public:
    enum Flag
    {
        RAW = 1,
        JSON = 2,
        FRAGMENT = 4,
        COMPRESSED = 8,
        DBOP = 16,
        PING = 32,
        RESERVED_1 = 64,
        SETUP = 128
    };
    Q_DECLARE_FLAGS( Flags, Flag );

    static MsgPtr create( const QByteArray& payload, const quint8 flags, const bool isCompressed = true );
    static quint8 lengthFieldLength();
    static quint8 flagsFieldLength();
    static quint8 headerLength();
    static quint32 maximumUncompressedMsgLength();

    quint32 payloadLength() const;
    bool hasFlag( const Flags &flag ) const;
    const QByteArray payload() const;
    Flags flags() const;
    const QByteArray serialize() const;

private:
    explicit Msg( const QByteArray& payload, const quint8 flags, const bool isCompressed = false, QObject *parent = 0 );

    MsgImplPtr m_impl;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Msg::Flags);

#endif // MSG_H
