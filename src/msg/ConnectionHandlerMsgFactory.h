/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef CONNECTIONHANDLERMSGFACTORY_H
#define CONNECTIONHANDLERMSGFACTORY_H

#include "MsgTypedefs.h"

class ConnectionHandlerMsgFactory
{
public:
    static const MsgPtr createAcceptPrimaryConnectionMessage( const QString &nodeId,
                                                              const QString &key,
                                                              const QString &port );

    static const MsgPtr createAcceptSecondaryConnectionMessage( const QString &controlId,
                                                                const QString &key,
                                                                const QString &port );

    static const MsgPtr createRequestConnectionMessage( const QString &controlId,
                                                        const QString &key,
                                                        const QString &offerKey,
                                                        const QString &port );

    static const MsgPtr createRequestConnectionResponse( const QString &controlId,
                                                         const QString &key,
                                                         const QString &port );
private:
    explicit ConnectionHandlerMsgFactory() {}
};

#endif // CONNECTIONHANDLERMSGFACTORY_H
