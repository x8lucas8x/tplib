/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "StreamConnectionMsgFactory.h"

#include "Msg.h"
#include "BufferIODevice.h"

#include <QIODevice>

const MsgPtr
StreamConnectionMsgFactory::createStreamDataMessage( const QIODevicePtr &stream )
{
    QByteArray payload = "data";
    payload.append( stream->read( BufferIODevice::blockSize() ) );

    if( stream->atEnd() )
    {
        return MsgPtr( Msg::create( payload, Msg::RAW, false ) );
    }
    else
    {
        // More messages to come.
        return MsgPtr( Msg::create( payload, Msg::RAW | Msg::FRAGMENT, false ) );
    }
}

const MsgPtr
StreamConnectionMsgFactory::createBlockRequestMessage( const quint32 block )
{
    QByteArray payload;
    payload.append( QString( "block%1" ).arg( block ) );

    return MsgPtr( Msg::create( payload, Msg::RAW | Msg::FRAGMENT, false ) );
}

const MsgPtr
StreamConnectionMsgFactory::createDoneBlockRequestMessage( const quint32 block )
{
    QByteArray payload;
    payload.append( QString( "doneblock%1" ).arg( block ) );

    return MsgPtr( Msg::create( payload, Msg::RAW | Msg::FRAGMENT, false ) );
}
