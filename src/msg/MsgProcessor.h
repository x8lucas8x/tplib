/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef MSGPROCESSOR_H
#define MSGPROCESSOR_H

#include "MsgTypedefs.h"

#include <QObject>
#include <QMap>

typedef QMap<QString, QVariant> QVariantMap;

class MsgProcessor : public QObject
{
    Q_OBJECT
public:
    enum Error
    {
        InvalidSetupMessage,
        InvalidJsonMessage,
        UnknowJsonObject,
        InvalidRawMessage,
        UnknownRawMessage,
        InvalidDatabaseOperationMessage,
        UnknowDatabaseOperationMessage,
        UnaceptableFlagsCombination
     };

    explicit MsgProcessor( QObject *parent = 0 );

    void startMessageProcessing( const MsgPtr &msg );

signals:
    void failedToProcessMessage( const MsgPtr &msg, const MsgProcessor::Error &errorType );

protected:
    virtual void processMessage( const MsgPtr &msg ) = 0;
    virtual bool isValidMessage( const MsgPtr &msg ) = 0;
};

#endif // MSGPROCESSOR_H
