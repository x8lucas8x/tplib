/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "ControlConnectionMsgProcessor.h"

#include "Msg.h"
#include "MsgProcessingUtilityFunctions.h"
#include <qjson/parser.h>

#include <QVariantMap>

#include "Debug.h"
#define DEBUG_FULL

ControlConnectionMsgProcessor::ControlConnectionMsgProcessor( QObject *parent )
    : ConnectionMsgProcessor( parent )
{
}

ControlConnectionMsgProcessorPtr
ControlConnectionMsgProcessor::create()
{
    return ControlConnectionMsgProcessorPtr( new ControlConnectionMsgProcessor );
}

void
ControlConnectionMsgProcessor::processMessage( const MsgPtr &msg )
{
    if( msg->hasFlag( Msg::PING ) )
    {
        // Used in ControlConnections on empty ping messages that are
        // periodically sent by both sides.
        emit pingReceived();
    }
    else if( msg->hasFlag( Msg::SETUP ) )
    {
        // Set in the version check and version response messages.
        processSetupMessage( msg );
    }
    if( msg->hasFlag( Msg::JSON ) )
    {
        // The msg payload is JSON representing a javascript object.
        processJsonMessage( msg );
    }
    else
    {
        emit failedToProcessMessage( msg, MsgProcessor::UnaceptableFlagsCombination );
    }
}

bool
ControlConnectionMsgProcessor::isValidMessage( const MsgPtr &msg )
{
    return msg->hasFlag( Msg::PING ) ||
           msg->hasFlag( Msg::SETUP ) ||
           msg->hasFlag( Msg::JSON ) ||
           msg->hasFlag( Msg::COMPRESSED );
}

void
ControlConnectionMsgProcessor::processJsonMessage( const MsgPtr &msg )
{
    DEBUG_BLOCK

    if( !( msg->hasFlag( Msg::JSON ) && MsgProcessingUtils::isValidJson( msg->payload() ) ) )
    {
        emit failedToProcessMessage( msg, MsgProcessor::InvalidJsonMessage );
        return;
    }

    const QVariantMap &json = MsgProcessingUtils::parseJson( msg->payload() );

    if ( !json.value( "method" ).isNull() )
    {
        // Execute a requested method.
        const QString method = json.value( "method" ).toString();

         if( method == "dbsync-offer" )
             emit startDatabaseSyncing( json.value( "key" ).toString() );
         else
             emit failedToProcessMessage( msg, MsgProcessor::UnknowJsonObject );
    }
    else
        emit failedToProcessMessage( msg, MsgProcessor::UnknowJsonObject );
}
