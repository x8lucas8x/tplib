/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef CONNECTIONHANDLERMSGPROCESSOR_H
#define CONNECTIONHANDLERMSGPROCESSOR_H

#include "MsgTypedefs.h"
#include "MsgProcessor.h"

#include <QObject>

class ConnectionHandlerMsgProcessor : public MsgProcessor
{
    Q_OBJECT
public:
    static ConnectionHandlerMsgProcessorPtr create();

signals:
    void requestControlConnectionCreation( const QString &key,
                                           const QString &nodeId );
    void requestSecondaryConnectionCreation( const QString &key,
                                             const QString &controlId );
    void receivedRequestForReverseConnection( const QString &key,
                                              const QString &controlId,
                                              const QString &offerKey );

protected:
    virtual void processMessage( const MsgPtr &msg );
    virtual bool isValidMessage( const MsgPtr &msg );

private:
    explicit ConnectionHandlerMsgProcessor( QObject *parent = 0 );   

    void processJsonMessage( const MsgPtr &msg );
};

#endif // CONNECTIONHANDLERMSGPROCESSOR_H
