/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "StreamConnectionMsgProcessor.h"

#include "Msg.h"

#include "Debug.h"
#define DEBUG_FULL

StreamConnectionMsgProcessor::StreamConnectionMsgProcessor( QObject *parent )
    : ConnectionMsgProcessor( parent )
{
}

StreamConnectionMsgProcessorPtr
StreamConnectionMsgProcessor::create()
{
    return StreamConnectionMsgProcessorPtr( new StreamConnectionMsgProcessor );
}

void
StreamConnectionMsgProcessor::processMessage( const MsgPtr &msg )
{
    if( msg->hasFlag( Msg::SETUP ) )
    {
        // Set in the version check and version response messages.
        processSetupMessage( msg );
    }
    else if( msg->hasFlag( Msg::RAW ) )
    {
        // Set on messages sent in StreamConnections after the initial message
        // and SETUP messages.
        processRawMessages( msg );
    }
    else
    {
        emit failedToProcessMessage( msg, MsgProcessor::UnaceptableFlagsCombination );
    }
}

bool
StreamConnectionMsgProcessor::isValidMessage( const MsgPtr &msg )
{
    return msg->hasFlag( Msg::SETUP ) ||
           msg->hasFlag( Msg::RAW );
}

void
StreamConnectionMsgProcessor::processRawMessages( const MsgPtr &msg )
{
    if( !msg->hasFlag( Msg::RAW ) )
    {
        emit failedToProcessMessage( msg, MsgProcessor::InvalidRawMessage );
        return;
    }

    if( msg->hasFlag( Msg::FRAGMENT ) )
    {
        if ( msg->payload().startsWith( "block" ) )
        {
            const quint32 block = QString( msg->payload() ).mid( 5 ).toInt();
            emit seekClientBufferToBlock( block );
        }
        else if ( msg->payload().startsWith( "doneblock" ) )
        {
            const quint32 block = QString( msg->payload() ).mid( 9 ).toInt();
            emit seekServerBufferToBlock( block );
        }
        else
            emit failedToProcessMessage( msg, MsgProcessor::UnknownRawMessage );
    }
    else if ( msg->payload().startsWith( "data" ) )
    {
        if( msg->hasFlag( Msg::FRAGMENT ) )
            emit newDataAvailable( msg->payload().mid( 4 ) );
        else
           emit lastRawMessageReceivedInBatch( msg->payload().mid( 4 ) );
    }
    else
        emit failedToProcessMessage( msg, MsgProcessor::UnknownRawMessage );
}
