/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef STREAMCONNECTIONMSGFACTORY_H
#define STREAMCONNECTIONMSGFACTORY_H

#include "MsgTypedefs.h"
#include "ConnectionMsgFactory.h"

class StreamConnectionMsgFactory : public ConnectionMsgFactory
{
public:
    static const MsgPtr createStreamDataMessage( const QIODevicePtr &stream );
    static const MsgPtr createBlockRequestMessage( const quint32 block );
    static const MsgPtr createDoneBlockRequestMessage( const quint32 block );

private:
    explicit StreamConnectionMsgFactory() {}
};

#endif // STREAMCONNECTIONMSGFACTORY_H
