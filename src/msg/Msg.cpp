/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "Msg.h"

#include <QtEndian>

#include "Debug.h"

#define DEBUG_FULL

//------------------------------------------------------------------------------
class MsgImpl
{
public:
    MsgImpl( const QByteArray &_payload, const quint8 _flags, const bool _isCompressed )
        : payload( _payload )
        , flags( _flags )
    {
        if( ( _isCompressed ) &&
            !( _flags & Msg::COMPRESSED ) &&
            ( _payload.length() > Msg::maximumUncompressedMsgLength() ) )
        {
            payload = qCompress( _payload, 9 );
            flags |= Msg::COMPRESSED;
        }
    }

    QByteArray payload;
    Msg::Flags flags;
};
//------------------------------------------------------------------------------

Msg::Msg( const QByteArray &payload, const quint8 flags, const bool isCompressed, QObject *parent )
    : QObject( parent )
    , m_impl( MsgImplPtr( new MsgImpl( payload, flags, isCompressed ) ) )
{
}

MsgPtr
Msg::create( const QByteArray &payload, const quint8 flags, const bool isCompressed )
{
    return MsgPtr( new Msg( payload, flags, isCompressed ) );
}

quint8
Msg::lengthFieldLength()
{
    return sizeof(quint32);
}

quint8
Msg::flagsFieldLength()
{
    return sizeof(m_impl->flags);
}

quint8
Msg::headerLength()
{
    return flagsFieldLength() + lengthFieldLength();
}

quint32
Msg::maximumUncompressedMsgLength()
{
    return 1024;
}

quint32
Msg::payloadLength() const
{    
    return m_impl->payload.length();
}

bool
Msg::hasFlag( const Flags &flag ) const
{
    return m_impl->flags & flag;
}

const QByteArray
Msg::payload() const
{
    if( hasFlag( Msg::COMPRESSED ) )
        return qUncompress( m_impl->payload );

    return m_impl->payload;
}

Msg::Flags
Msg::flags() const
{
    return m_impl->flags;
}

const QByteArray
Msg::serialize() const
{
    return QByteArray()
            .append( qToBigEndian( payloadLength() + headerLength() ) )
            .append( m_impl->flags )
            .append( m_impl->payload );
}
