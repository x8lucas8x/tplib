/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "DatabaseSyncConnectionMsgProcessor.h"

#include "Msg.h"
#include "MsgProcessingUtilityFunctions.h"
#include "DatabaseCommandFactoryWorker.h"
#include "DatabaseCommandUtilityFunctions.h"
#include "DatabaseCommandStrings.h"
#include "AddFilesCommand.h"
#include "DeleteFilesCommand.h"
#include "CreatePlaylistCommand.h"
#include "DeletePlaylistCommand.h"
#include "RenamePlaylistCommand.h"
#include "LogPlaybackCommand.h"
#include "SocialActionCommand.h"
#include "SetPlaylistRevisionCommand.h"

#include <QVariantMap>

#include "Debug.h"
#define DEBUG_FULL

//------------------------------------------------------------------------------
class DatabaseSyncConnectionMsgProcessorImpl
{
public:
    DatabaseSyncConnectionMsgProcessorImpl()
        : lastDatabaseOperationGuid( "" )
    {
    }

    MsgListPtr databaseOperationMessages;
    DatabaseCommandFactoryWorker *databaseCommandFactory;
    QString lastDatabaseOperationGuid;
};
//------------------------------------------------------------------------------

DatabaseSyncConnectionMsgProcessor::DatabaseSyncConnectionMsgProcessor( QObject *parent )
    : ConnectionMsgProcessor( parent )
    , m_impl( DatabaseSyncConnectionMsgProcessorImplPtr( new DatabaseSyncConnectionMsgProcessorImpl ) )
{
}

DatabaseSyncConnectionMsgProcessorPtr
DatabaseSyncConnectionMsgProcessor::create()
{
    return DatabaseSyncConnectionMsgProcessorPtr( new DatabaseSyncConnectionMsgProcessor );
}

void
DatabaseSyncConnectionMsgProcessor::processMessage( const MsgPtr &msg )
{
    if( msg->hasFlag( Msg::SETUP ) )
    {
        // Set in the version check and version response messages.
        processSetupMessage( msg );
    }
    else if( msg->hasFlag( Msg::DBOP ) )
    {
        // A DB OPeration sent in DBSyncConnections.
        processDatabaseOperationMessage( msg );
    }
    else if( msg->hasFlag( Msg::JSON ) )
    {
        // The msg payload is JSON representing a javascript object.
        processJsonMessage( msg );
    }
    else
    {
        emit failedToProcessMessage( msg, MsgProcessor::UnaceptableFlagsCombination );
    }
}

bool
DatabaseSyncConnectionMsgProcessor::isValidMessage( const MsgPtr &msg )
{
    return msg->hasFlag( Msg::SETUP ) ||
            msg->hasFlag( Msg::JSON ) ||
            msg->hasFlag( Msg::DBOP ) ||
            msg->hasFlag( Msg::COMPRESSED );
}

void
DatabaseSyncConnectionMsgProcessor::processJsonMessage( const MsgPtr &msg )
{
    DEBUG_BLOCK

    if( !( msg->hasFlag( Msg::JSON ) && MsgProcessingUtils::isValidJson( msg->payload() ) ) )
    {
        emit failedToProcessMessage( msg, MsgProcessor::InvalidJsonMessage );
        return;
    }

    const QVariantMap &json = MsgProcessingUtils::parseJson( msg->payload() );

    if ( !json.value( "method" ).isNull() )
    {
        // Execute a requested method.
        const QString method = json.value( "method" ).toString();

         if( method == "fetchops" )
             emit fetchDatabaseOperations( json.value( "key" ).toString() );
         if( method == "trigger" )
             emit triggerDatabaseOperationFetches();
         else
             emit failedToProcessMessage( msg, MsgProcessor::UnknowJsonObject );
    }
    else
        emit failedToProcessMessage( msg, MsgProcessor::UnknowJsonObject );
}

void
DatabaseSyncConnectionMsgProcessor::processDatabaseOperationMessage( const MsgPtr &msg )
{
    DEBUG_BLOCK

    if( !msg->hasFlag( Msg::DBOP ) )
    {
        emit failedToProcessMessage( msg, MsgProcessor::InvalidDatabaseOperationMessage );
        return;
    }

    // Everything is synced, indicated by non-json msg containing "ok".
    if( msg->payload() == "ok" )
        emit databaseSynced( m_impl->lastDatabaseOperationGuid );

    m_impl->databaseOperationMessages.append( msg );

    if ( !msg->hasFlag( Msg::FRAGMENT ) )
    {
        DatabaseCommandFactoryWorker *factory = new DatabaseCommandFactoryWorker;
        m_impl->databaseCommandFactory = factory;
        factory->doWork( m_impl->databaseOperationMessages );

        connect( factory, SIGNAL(tracksAdded(TPCoreApi::TrackPtrList)),
                 this, SIGNAL(tracksAdded(TPCoreApi::TrackPtrList)) );
        connect( factory, SIGNAL(tracksRemoved(QList<qulonglong>)),
                 this, SIGNAL(tracksRemoved(QList<qulonglong>)) );
        connect( factory, SIGNAL(playlistsCreated(TPCoreApi::PlaylistPtrList)),
                 this, SIGNAL(playlistsCreated(TPCoreApi::PlaylistPtrList)) );
        connect( factory, SIGNAL(playlistsDeleted(QList<QString>)),
                 this, SIGNAL(playlistsDeleted(QList<QString>)) );
        connect( factory, SIGNAL(playlistRenamed(QString,QString)),
                 this, SIGNAL(playlistRenamed(QString,QString)) );
        connect( factory, SIGNAL(playlistChanged(QString,TPCoreApi::PlaylistRevisionPtr)),
                 this, SIGNAL(playlistChanged(QString,TPCoreApi::PlaylistRevisionPtr)) );
        connect( factory, SIGNAL(playbackStarted(QString,QString)),
                 this, SIGNAL(playbackStarted(QString,QString)) );
        connect( factory, SIGNAL(playbackFinished(QString,QString,quint32)),
                 this, SIGNAL(playbackFinished(QString,QString,quint32)) );
        connect( factory, SIGNAL(trackLoved(QString,QString)),
                 this, SIGNAL(trackLoved(QString,QString)) );
        connect( factory, SIGNAL(trackUnloved(QString,QString)),
                 this, SIGNAL(trackUnloved(QString,QString)) );
        connect( factory, SIGNAL(finished(QString)),
                 factory, SLOT(deleteLater()) );
        connect( factory, SIGNAL(finished(QString)),
                 factory, SLOT(onLastDatabaseCommandReceivedInBatch(QString)) );

        m_impl->databaseOperationMessages.clear();
    }
}


void
DatabaseSyncConnectionMsgProcessor::onLastDatabaseCommandReceivedInBatch( const QString lastDatabaseOperationGuid )
{
    m_impl->lastDatabaseOperationGuid = lastDatabaseOperationGuid;
}
