/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "ConnectionHandlerMsgProcessor.h"

#include "Msg.h"
#include "MsgProcessingUtilityFunctions.h"
#include <qjson/parser.h>

#include <QVariantMap>

#include "Debug.h"
#define DEBUG_FULL

ConnectionHandlerMsgProcessor::ConnectionHandlerMsgProcessor( QObject *parent )
    : MsgProcessor( parent )
{
}

ConnectionHandlerMsgProcessorPtr
ConnectionHandlerMsgProcessor::create()
{
    return ConnectionHandlerMsgProcessorPtr( new ConnectionHandlerMsgProcessor() );
}

void
ConnectionHandlerMsgProcessor::processMessage( const MsgPtr &msg )
{
    DEBUG_BLOCK

    // COMPRESSED: The payload has been compressed using QT's qCompress function
    //   which I think uses zlib. To uncompress using python's zlib module you
    //   first need to remove the initial 4 byte header which contains a
    //   big-endian integer that is the uncompressed size of the payload in bytes.
    //   The compressed flag is only used and supported in ControlConnections and
    //   DBSyncConnections.
    // FRAGMENT: Indicates that a command will be followed by more. When sending
    //   db operations, all but the last DBOP message will be flagged with
    //   FRAGMENT. Similarly when streaming a song all but the last message will
    //   have the FRAGMENT flag set.

    if( msg->hasFlag( Msg::JSON ) )
    {
        // The msg payload is JSON representing a javascript object.
        processJsonMessage( msg );
    }
    else
    {
        emit failedToProcessMessage( msg, MsgProcessor::UnaceptableFlagsCombination );
    }
}

bool
ConnectionHandlerMsgProcessor::isValidMessage( const MsgPtr &msg )
{
    return msg->hasFlag( Msg::JSON );
}

void
ConnectionHandlerMsgProcessor::processJsonMessage( const MsgPtr &msg )
{
    DEBUG_BLOCK

    if( !( msg->hasFlag( Msg::JSON ) && MsgProcessingUtils::isValidJson( msg->payload() ) ) )
    {
        emit failedToProcessMessage( msg, MsgProcessor::InvalidJsonMessage );
        return;
    }

    const QVariantMap &json = MsgProcessingUtils::parseJson( msg->payload() );

    if( !json.value( "conntype" ).isNull() )
    {
        // Initiate a Connection.
        const QString connectionType = json.value( "conntype" ).toString();
        const QString key = json.value( "key" ).toString();
        const QString nodeId = json.value( "nodeid" ).toString();
        const QString controlId = json.value( "controlid" ).toString();

        if( ( connectionType == "accept-offer" ) ||
                ( connectionType == "push-offer" ) )
        {
            // A peer connected to us.
            if( !nodeId.isEmpty() )
            {
                // Only control connections send nodeId field.
                emit requestControlConnectionCreation( key, nodeId );
            }
            else if ( !controlId.isEmpty() )
            {
                // Only DBSync connections and stream connections send controlId field.
                emit requestSecondaryConnectionCreation( key, controlId );
            }
            else
                emit failedToProcessMessage( msg, MsgProcessor::UnknowJsonObject );
        }
        else if( connectionType == "request-offer" )
        {
            // A peer is requesting a reverse connection.
            const QString offerKey = json.value( "offer" ).toString();
            emit receivedRequestForReverseConnection( key, controlId, offerKey );
        }
        else
            emit failedToProcessMessage( msg, MsgProcessor::UnknowJsonObject );
    }
    else
        emit failedToProcessMessage( msg, MsgProcessor::UnknowJsonObject );
}
