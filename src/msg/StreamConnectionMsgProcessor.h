/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef STREAMCONNECTIONMSGPROCESSOR_H
#define STREAMCONNECTIONMSGPROCESSOR_H

#include "MsgTypedefs.h"
#include "ConnectionMsgProcessor.h"

class StreamConnectionMsgProcessor : public ConnectionMsgProcessor
{
    Q_OBJECT
public:
    static StreamConnectionMsgProcessorPtr create();

signals:
    void seekClientBufferToBlock( const quint32 fileBlock );
    void seekServerBufferToBlock( const quint32 fileBlock );
    void newDataAvailable( const QByteArray &fileData );
    void lastRawMessageReceivedInBatch( const QByteArray &fileData );

protected:
    virtual void processMessage( const MsgPtr &msg );
    virtual bool isValidMessage( const MsgPtr &msg );

private:
    explicit StreamConnectionMsgProcessor( QObject *parent = 0 );

    void processRawMessages( const MsgPtr &msg );
};

#endif // STREAMCONNECTIONMSGPROCESSOR_H
