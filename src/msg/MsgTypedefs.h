/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef MSGTYPEDEFS_H
#define MSGTYPEDEFS_H

#include <QSharedPointer>

class QIODevice;
typedef QSharedPointer<QIODevice> QIODevicePtr;

class Msg;
typedef QSharedPointer<Msg> MsgPtr;
class MsgImpl;
typedef QSharedPointer<MsgImpl> MsgImplPtr;
typedef QList<MsgPtr> MsgListPtr;

class ConnectionMsgProcessor;
typedef QSharedPointer<ConnectionMsgProcessor> ConnectionMsgProcessorPtr;

class ConnectionHandlerMsgProcessor;
typedef QSharedPointer<ConnectionHandlerMsgProcessor> ConnectionHandlerMsgProcessorPtr;

class ControlConnectionMsgProcessor;
typedef QSharedPointer<ControlConnectionMsgProcessor> ControlConnectionMsgProcessorPtr;

class StreamConnectionMsgProcessor;
typedef QSharedPointer<StreamConnectionMsgProcessor> StreamConnectionMsgProcessorPtr;

class DatabaseSyncConnectionMsgProcessor;
typedef QSharedPointer<DatabaseSyncConnectionMsgProcessor> DatabaseSyncConnectionMsgProcessorPtr;
class DatabaseSyncConnectionMsgProcessorImpl;
typedef QSharedPointer<DatabaseSyncConnectionMsgProcessorImpl> DatabaseSyncConnectionMsgProcessorImplPtr;

#endif // MSGTYPEDEFS_H
