/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef CONTROLCONNECTIONMSGPROCESSOR_H
#define CONTROLCONNECTIONMSGPROCESSOR_H

#include "ConnectionMsgProcessor.h"

#include "Msg.h"

class ControlConnectionMsgProcessor : public ConnectionMsgProcessor
{
    Q_OBJECT
public:
    static ControlConnectionMsgProcessorPtr create();
    
signals:
    void pingReceived();
    void startDatabaseSyncing( const QString &requesterUUID );

protected:
    virtual void processMessage( const MsgPtr &msg );
    virtual bool isValidMessage( const MsgPtr &msg );

private:
    explicit ControlConnectionMsgProcessor( QObject *parent = 0 );

    void processJsonMessage( const MsgPtr &msg );
};

#endif // CONTROLCONNECTIONMSGPROCESSOR_H
