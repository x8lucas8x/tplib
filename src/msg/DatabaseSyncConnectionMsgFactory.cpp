/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "DatabaseSyncConnectionMsgFactory.h"

#include "DatabaseCommand.h"
#include "Msg.h"
#include "MsgProcessingUtilityFunctions.h"

#include <QVariantMap>

const MsgPtr
DatabaseSyncConnectionMsgFactory::createFetchDatabaseOperationsMessage( const QString &lastOps )
{
    QVariantMap json;

    json["method"] = "fetchops";
    json["lastop"] = lastOps;

    return MsgPtr( Msg::create( MsgProcessingUtils::serializeJson( json ), Msg::JSON ) );
}

const MsgPtr
DatabaseSyncConnectionMsgFactory::createTriggerFetchDatabaseOperationsMessage()
{
    QVariantMap json;

    json["method"] = "trigger";

    return MsgPtr( Msg::create( MsgProcessingUtils::serializeJson( json ), Msg::JSON ) );
}

const MsgPtr
DatabaseSyncConnectionMsgFactory::createNoNewDatabaseCommandsMessage()
{
    return MsgPtr( Msg::create( "ok", Msg::DBOP ) );
}

const MsgListPtr
DatabaseSyncConnectionMsgFactory::createSendDatabaseOperationsDataMessages( const DatabaseCommandPtrList &commands )
{
    MsgListPtr msgs;
    QByteArray msgPayload;
    quint32 i;

    for( i = 0; i < (commands.length()-1); ++i )
    {
        msgPayload = MsgProcessingUtils::serializeJson( commands[i]->databaseCommandToJson() );

        msgs.append( Msg::create( msgPayload, Msg::JSON | Msg::DBOP | Msg::FRAGMENT ) );
    }

    msgPayload = MsgProcessingUtils::serializeJson( commands[i]->databaseCommandToJson() );

    msgs.append( Msg::create( msgPayload, Msg::JSON | Msg::DBOP ) );
    return msgs;
}
