/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#ifndef DATABASESYNCCONNECTIONMSGPROCESSOR_H
#define DATABASESYNCCONNECTIONMSGPROCESSOR_H

#include "MsgTypedefs.h"
#include "DatabaseCommandsTypedefs.h"
#include "CoreApiTypedefs.h"
#include "ConnectionMsgProcessor.h"

class DatabaseSyncConnectionMsgProcessor : public ConnectionMsgProcessor
{
    Q_OBJECT
public:
    static DatabaseSyncConnectionMsgProcessorPtr create();

protected:
    virtual void processMessage( const MsgPtr &msg );
    virtual bool isValidMessage( const MsgPtr &msg );

signals:
    void databaseSynced( const QString lastDatabaseOperationGuid );
    void fetchDatabaseOperations( const QString lastDatabaseOperationGuid );
    void triggerDatabaseOperationFetches();

    void tracksAdded( const TPCoreApi::TrackPtrList tracks );
    void tracksRemoved( const QList<qulonglong> trackIds );
    void playlistsCreated( const TPCoreApi::PlaylistPtrList playlists );
    void playlistsDeleted( const QList<QString> playlistIds );
    void playlistChanged( const QString playlistId, const TPCoreApi::PlaylistRevisionPtr revision );
    void playlistRenamed( const QString playlistId, const QString &newTitle );
    void playbackStarted( const QString &artistName, const QString &trackName );
    void playbackFinished( const QString &artistName, const QString &trackName, const quint32 secondsPlayed );
    void trackLoved( const QString &artistName, const QString &trackName );
    void trackUnloved( const QString &artistName, const QString &trackName );

private slots:
    void onLastDatabaseCommandReceivedInBatch( const QString lastDatabaseOperationGuid );

private:    
    explicit DatabaseSyncConnectionMsgProcessor( QObject *parent = 0 );

    DatabaseSyncConnectionMsgProcessorImplPtr m_impl;

    void processJsonMessage( const MsgPtr &msg );
    void processDatabaseOperationMessage( const MsgPtr &msg );
};

#endif // DATABASESYNCCONNECTIONMSGPROCESSOR_H
