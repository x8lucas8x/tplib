/****************************************************************************************
 * Copyright (c) 2013 Lucas Lira Gomes<x8lucas8x@gmail.com>                             *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

#include "ConnectionMsgProcessor.h"

#include "Msg.h"

#include "Debug.h"
#define DEBUG_FULL

ConnectionMsgProcessor::ConnectionMsgProcessor( QObject *parent )
    : MsgProcessor( parent )
{
}

void
ConnectionMsgProcessor::processSetupMessage( const MsgPtr &msg )
{
    if( !msg->hasFlag( Msg::SETUP ) )
    {
        emit failedToProcessMessage( msg, MsgProcessor::InvalidSetupMessage );
        return;
    }

    // Set in the version check and version response messages.
    if( msg->payload() == QString::number( TOMAHAWK_PROTOCOL_VERSION ) )
        emit setupAndVersionCheckSucceed();
    else
        emit setupAndVersionCheckFailed();
}
